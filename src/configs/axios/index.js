import axios from 'axios';
import errorHandler from './errorHandler';

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_EMPLOYEE_URL,
  headers: {
    'Content-Type': 'application/json',
    'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_EMPLOYEE_HEADER_AUTH,
  },
});

instance.interceptors.response.use((response) => response.data, errorHandler);

export default instance;
