export default function errorHandler(error) {
  if (error) {
    let message;
    if (error.response) {
      if (error.response.status === 500) {
        message = 'Something went wrong';
      } else message = error.response.data.message ?? '';

      const formatError = {
        status: error.response.status,
        message,
        errors: error.response.data.errors ?? [],
        data: error.response.data.data ?? null,
      };

      return Promise.reject(formatError);
    }
  }
}
