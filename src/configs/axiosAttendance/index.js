import axios from 'axios';
import errorHandler from './errorHandler';

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_ATTENDANCE_URL,
  headers: {
    'Content-Type': 'application/json',
    'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_ATTENDANCE_HEADER_AUTH,
  },
});

instance.interceptors.response.use((response) => response, errorHandler);

export default instance;
