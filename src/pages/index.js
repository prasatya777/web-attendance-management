import dynamic from 'next/dynamic';
import React from 'react';
const PageDashboard = dynamic(() => import('@components/dashboard/page-dashboard'));

export default function index() {
  return (
    <div>
      <PageDashboard />
    </div>
  );
}
