import { withSession } from '@utils/sessions';
import axios from '@configs/axios';

const handler = async (req, res) => {
  const token = req.session.get('TOKEN');

  if (token) {
    try {
      const { data } = await axios.get('/employees/me', {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      });

      res.json({
        data: { isLoggedIn: true, data, token },
      });
    } catch (err) {
      req.session.destroy();
      res.json({ data: { isLoggedIn: false } });
    }
  } else {
    res.json({ data: { isLoggedIn: false } });
  }
};

export default withSession(handler);
