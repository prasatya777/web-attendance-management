import { withSession } from '@utils/sessions';
import axios from '@configs/axios';

export const handler = async (req, res) => {
  try {
    // const token = req.session.get('TOKEN');

    // await axios.get(`/user/logout`, {
    //   headers: {
    //     Authorization: `Bearer ${token}`,
    //   },
    // });

    req.session.destroy();
    res.json({ isLoggedIn: false });
  } catch (error) {
    res.status(error.status).json(error);
  }
};

export default withSession(handler);
