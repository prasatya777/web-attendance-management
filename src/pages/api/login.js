import { withSession } from '@utils/sessions';
import axios from '@configs/axios';

const handler = async (req, res) => {
  const { email, password } = req.body;

  try {
    const response = await axios.post(`/employees/login`, {
      email,
      password,
    });

    req.session.set('TOKEN', response.data.token);
    await req.session.save();
    res.json({ isLoggedIn: true });
  } catch (error) {
    console.log(error);
    res.status(error.status).json(error);
  }
};

export default withSession(handler);
