import dynamic from 'next/dynamic';

import { useConfigUserLevel } from '@data/user-management/useUserManagement';
import { useUser } from '@data/useUser';
import { configUserLevel } from '@utils/configUserLevel';
import React from 'react';
import ErrorPage from './401';
const PageDashboard = dynamic(() => import('@components/dashboard/page-dashboard'));
export default function Dashboard() {
  const { user } = useUser({});

  // NOTE handler access level page
  const { data: isHaveAccess, loading: loadingCheckAccess } = useConfigUserLevel({
    userToken: user?.token,
    params: {
      nameConfig: configUserLevel.MENU_DASHBOARD,
    },
  });

  if (!loadingCheckAccess) {
    if (!isHaveAccess) {
      return <ErrorPage statusCode={401} />;
    }
  }

  return (
    <div>
      <PageDashboard />
    </div>
  );
}
