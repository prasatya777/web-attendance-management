import { useEffect, useState } from 'react';
import dynamic from 'next/dynamic';
import { useConfigUser } from '@data/employee/useEmployee';
import { useAttendanceHistory } from '@data/attendance/useAttendance';
import { useUser } from '@data/useUser';
import { configUserLevel } from '@utils/configUserLevel';

const SectionTitle = dynamic(() => import('@components/dashboard/section-title'));
const TableListAttendance = dynamic(() =>
  import('@components/Attendance/table-list-attendance'),
);
const ModalFilter = dynamic(() =>
  import('@components/Attendance/modals/modalFilter'),
);

const Pagination = dynamic(() => import('@components/pagination/pagination'));
const ErrorPage = dynamic(() => import('@pages/401'));

const AttendanceHistory = () => {
  const { user, loading: loadingUser } = useUser({});

  const initValueFilter = {
    dateTime: {
      startDate: '',
      endDate: '',
    },
  };

  const [initialValuesFilter, setInitialValuesFilter] = useState(initValueFilter);
  const [filter, setFilter] = useState({ ...initValueFilter, page: 1 });
  const [loadingListEmployee, setLoadingListEmployee] = useState(true);
  const [isModalFilterOpen, setIsModalFilterOpen] = useState(false);

  const {
    data: dataListAttendanceHistory,
    mutate: mutateListEmployee,
  } = useAttendanceHistory(user?.token, filter);

  const handlerPageChange = async ({ selected: currentPage }) => {
    setFilter({ ...filter, page: currentPage + 1 });
    await mutateListEmployee();
  };

  useEffect(() => {
    if (dataListAttendanceHistory) {
      setLoadingListEmployee(false)
    }
  }, [dataListAttendanceHistory])


  // SECTION handler access level page
  const { data: isHaveAccess } = useConfigUser({
    userToken: user?.token,
    params: {
      nameConfig: configUserLevel.MENU_EMPLOYEE_HR,
    },
  });

  if (!loadingUser) {
    if (!isHaveAccess) {
      return <ErrorPage statusCode={401} />;
    }
  }

  return (
    <>
      <SectionTitle title="" subtitle="Attendance History" />

      <button
        onClick={() => {
          setIsModalFilterOpen(true);
        }}
        className="bg-blue-500 flex items-center text-sm py-3 px-5 gap-x-3 rounded-lg text-white cursor-pointer"
      >
        FILTER
        <img src="/images/icon-filter.png" width={20} alt="add-user" />
      </button>

      <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0 mb-2 lg:my-4">
        <div className="'w-full lg:px-10 lg:py-8 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800'">
          <div className="flex mb-8">
            <p className="font-bold">Attendance History</p>
          </div>
          <div className="overflow-x-scroll lg:overflow-hidden">

          {!loadingUser && !loadingListEmployee && (
            <TableListAttendance
              data={dataListAttendanceHistory?.data ?? []}
              isLoading={loadingListEmployee}
            />
          )}

          {!loadingUser &&(
            <Pagination
              handlerPageChange={handlerPageChange}
              totalAllData={dataListAttendanceHistory?.paginate?.total_all_data ?? 0}
              itemsPerPage={dataListAttendanceHistory?.paginate?.show_record ?? 10}
            />
          )}
          
          </div>
        </div>
      </div>

      {/* Modal */}
      {isModalFilterOpen && (
        <ModalFilter
          initialValues={initialValuesFilter}
          isOpen={isModalFilterOpen}
          shouldCloseOnOverlayClick={true}
          onClose={() => setIsModalFilterOpen(false)}
          onSubmit={(values) => {
            setInitialValuesFilter(values);
            setIsModalFilterOpen(false);
            setFilter(values);
          }}
          onReset={() => {
            setInitialValuesFilter(initValueFilter);
            setIsModalFilterOpen(false);
            setFilter(initValueFilter);
          }}
        />
      )}
    </>
  );
};
export default AttendanceHistory;
