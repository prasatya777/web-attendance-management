import { useEffect, useState } from 'react';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import {
  useConfigUser,
  useEmployeeList,
} from '@data/employee/useEmployee';
import { useUser } from '@data/useUser';
import { configUserLevel } from '@utils/configUserLevel';

const Section = dynamic(() => import('@components/employee/section'));
const SectionTitle = dynamic(() => import('@components/dashboard/section-title'));
const TableListUser = dynamic(() =>
  import('@components/employee/table-list-user'),
);
const Pagination = dynamic(() => import('@components/pagination/pagination'));
const ErrorPage = dynamic(() => import('@pages/401'));

const EmployeeManagement = () => {
  const { user, loading: loadingUser } = useUser({});

  const [filter, setFilter] = useState({ page: 1 });
  const [loadingListEmployee, setLoadingListEmployee] = useState(true);

  const {
    data: dataListEmployee,
    mutate: mutateListEmployee,
  } = useEmployeeList(user?.token, filter);

  const handlerPageChange = async ({ selected: currentPage }) => {
    setFilter({ ...filter, page: currentPage + 1 });
    await mutateListEmployee();
  };

  useEffect(() => {
    if (dataListEmployee) {
      setLoadingListEmployee(false)
    }
  }, [dataListEmployee])


  // SECTION handler access level page
  const { data: isHaveAccess } = useConfigUser({
    userToken: user?.token,
    params: {
      nameConfig: configUserLevel.MENU_EMPLOYEE_HR,
    },
  });

  if (!loadingUser) {
    if (!isHaveAccess) {
      return <ErrorPage statusCode={401} />;
    }
  }

  return (
    <>
      <SectionTitle title="" subtitle="Employee Management" />

      <div className="grid lg:grid-cols-4 gap-x-3 gap-y-3 w-full mb-2 lg:mb-5">
        <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0">
          <Section
            title="TOTAL USER"
            description={dataListEmployee?.paginate?.total_all_data ?? 0}
            classname="h-full"
          />
        </div>
        <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0 mb-3 h-full">
          <Link href={'/employee/add'}>
            <a className="flex w-full h-full cursor-pointer rounded-lg bg-blue-500 text-white gap-x-3 items-center justify-center">
              <img src="/images/icon-user-plus.png" alt="add-user" />
              <p>BUAT EMPLOYEE BARU</p>
            </a>
          </Link>
        </div>
      </div>

      <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0 mb-2 lg:my-4">
        <div className="'w-full lg:px-10 lg:py-8 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800'">
          <div className="flex mb-8">
            <p className="font-bold">List User</p>
          </div>
          <div className="overflow-x-scroll lg:overflow-hidden">

          {!loadingUser && !loadingListEmployee && (
            <TableListUser
              data={dataListEmployee?.data ?? []}
              isLoading={loadingListEmployee}
            />
          )}

          {!loadingUser &&(
            <Pagination
              handlerPageChange={handlerPageChange}
              totalAllData={dataListEmployee?.paginate?.total_all_data ?? 0}
              itemsPerPage={dataListEmployee?.paginate?.show_record ?? 10}
            />
          )}
          </div>
        </div>
      </div>
    </>
  );
};
export default EmployeeManagement;
