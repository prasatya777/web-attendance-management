import { useRouter } from 'next/router';
import Link from 'next/link';
import { useEmployeeDetail } from '@data/employee/useEmployee';
import { useUser } from '@data/useUser';
import Head from 'next/head';
import ModalPictures from '@components/locations/modals/modalPictures';
import { useState } from 'react';

const getColorByStatus = (status) => {
  switch (status.toLowerCase()) {
    case 'active':
      return 'green-500';
    case 'inactive':
      return 'red-500';
    case 'draft':
      return 'gray-500';
    default:
      return 'black';
  }
};

const EmployeeDetail = () => {
  const router = useRouter();
  const { id } = router.query;

  const { user } = useUser({});

  const { data: dataEmployeeDetail } = useEmployeeDetail({
    userToken: user?.token,
    params: {
      uuidEmployee: id,
    },
  });

  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <>
      <Head>
        <title>Attendance Management | Employee Detail</title>
      </Head>
      <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0 mb-2 lg:my-4">
        <div className="w-full p-5 lg:px-10 lg:py-8 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800">
          <div className="flex flex-col gap-y-3 lg:gap-y-0 lg:flex-row lg:gap-x-7">
            <div className="lg:w-1/6">
              <div
                className="relative cursor-pointer"
                onClick={() => {
                  setIsModalOpen(true);
                }}
              >
                <img
                  src={dataEmployeeDetail?.image[0]}
                  alt="popcenter-image"
                  className="object-contain rounded-lg"
                />
                {dataEmployeeDetail?.image?.length > 1 && (
                  <div className="absolute top-0  w-full h-full flex items-end justify-center">
                    <div className="flex mb-2 gap-x-3">
                      <div className="w-3 h-3 rounded-full bg-blue-500"></div>
                      <div className="w-3 h-3 rounded-full bg-gray-400"></div>
                      <div className="w-3 h-3 rounded-full bg-gray-100"></div>
                    </div>
                  </div>
                )}
              </div>
            </div>

            <div className="lg:w-5/6">
              <div className="flex flex-col md:flex-row gap-y-3 md:gap-y-0 justify-between mb-5">
                <div className="flex items-center lg:items-end gap-x-4">
                  <div className="">
                    <p className="text-gray-400 text-xs">Nama</p>
                    <div className="flex flex-col lg:flex-row items-start lg:items-center gap-x-4">
                      <h3 className="font-bold text-lg lg:text-xl">
                        {dataEmployeeDetail?.name ?? '-'}
                      </h3>
                      <p
                        className={`px-5 py-1 font-bold text-white text-xs rounded-full bg-${getColorByStatus(
                          dataEmployeeDetail?.status ?? '',
                        )}`}
                      >
                        {dataEmployeeDetail?.status}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="flex gap-x-3">
                  <Link href={`/employee/edit/${id}`}>
                    <a className="bg-blue-500 inline-flex items-center text-sm py-3 px-5 gap-x-3 rounded-md text-white justify-center">
                      EDIT DATA
                      <img src="/images/ic-pencil.png" alt="edit" />
                    </a>
                  </Link>
                </div>
              </div>
              <div className="detail">
                <div className="mb-5">
                  <p className="text-gray-400 mb-1">Position Job</p>
                  <p>{dataEmployeeDetail?.positionJob}</p>
                </div>
                <div className="mb-5">
                  <p className="text-gray-400 mb-1">Email</p>
                  <p>{dataEmployeeDetail?.email}</p>
                </div>
                <div className="mb-5">
                  <p className="text-gray-400 mb-1">Phone Number</p>
                  <p>{dataEmployeeDetail?.phone}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {isModalOpen && (
        <ModalPictures
          isOpen={isModalOpen}
          onClose={() => setIsModalOpen(false)}
          shouldCloseOnOverlayClick={true}
          data={{
            images: dataEmployeeDetail?.image,
          }}
        />
      )}
    </>
  );
};
export default EmployeeDetail;
