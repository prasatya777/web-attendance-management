import SectionTitle from '@components/dashboard/section-title';
import { useEffect, useRef, useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { SelectReact } from '@components/forms/Input';
import Alert from '@components/alerts';
import { getErrorMessageResponse } from '@helpers/errorMessageFormatter';
import { useUser } from '@data/useUser';
import { addEmployee } from '@services/employee/employeeService';
import {
  useGenerateUuid,
} from '@data/employee/useEmployee'
import Head from 'next/head';

const LocationAdd = () => {
  const router = useRouter();

  const { user } = useUser({});
  const { data: uuidGenerate } = useGenerateUuid(user?.token);

  const initiateValues = {
    uuidEmployee: uuidGenerate?.uuid,
    name: '',
    email: '',
    positionJob: '',
    phone: '',
    password: '',
    type: '',
    status: '',
  };

  const [initialValues, setInitialValues] = useState(initiateValues)

  useEffect(() => {
    if (uuidGenerate) {
      setInitialValues({
        ...initialValues,
        uuidEmployee: uuidGenerate?.uuid,
      })
    }
  }, [uuidGenerate])
  
  const [uploadedPictures, setUploadedPictures] = useState([]);
  const [messageResponse, setMessageResponse] = useState(null);
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);

  const onSubmit = async (values) => {
    try {
      setIsLoadingSubmit(true)
      const payload = values;
      payload.images = uploadedPictures.map((image) => image.file);

      const formData = new FormData();
      for (const key in payload) {
        if (payload[key] instanceof Array) {
          if (key.toLowerCase() === 'images') {
            payload[key].forEach((item) => {
              formData.append(`${key}`, item);
            });
          } else {
            formData.append(`${key}`, JSON.stringify(payload[key]));
          }
        } else {
          formData.append(key, payload[key]);
        }
      }

      const dataTes = await addEmployee({
        token: user?.token,
        payload: formData,
      });
      if(dataTes === true){
        return router.push(`/employee/list`);
      }

      setMessageResponse('Failed Update Data Employee');
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });

    } catch (error) {

      console.error(error);
      const errorMessage = getErrorMessageResponse(error);
      setMessageResponse(errorMessage);
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }finally {
      setIsLoadingSubmit(false)
    }
  };

  // handling location picture
  const [messageUploadPicture, setMessageUploadPicture] = useState('');
  const refUpload = useRef(null);

  const handleUploadImage = () => {
    refUpload?.current?.click();
  };

  const handleOnSelectedPicture = (e) => {
    const files = e.currentTarget.files;
    if (uploadedPictures.length > 2) {
      setMessageUploadPicture(`Maximum 3 pictures allowed`);
      return;
    } else {
      if (files.length > 0) {
        // const tempPictures = [];
        const file = files[0];
        const allowedMaxFileSize = process.env.NEXT_PUBLIC_LIMIT_IMAGE;

        if (file.size > allowedMaxFileSize) {
          setMessageUploadPicture(
            `File size must less than ${allowedMaxFileSize} KB`,
          );

          return;
        }
        const url = URL.createObjectURL(file);
        setUploadedPictures([...uploadedPictures, { file, url }]);
      }
    }
    e.target.value = '';
  };

  const handleRemoveImage = (index) => {
    const newPictures = uploadedPictures.filter((_, i) => i !== index);

    setUploadedPictures(newPictures);
  };

  const [showAlert, setShowAlert] = useState(true);
  return (
    <>
      <Head>
        <title>Attendance Management | Add Profile</title>
      </Head>
      {messageResponse && (
        <Alert
          color="bg-transparent border-red-500 text-red-500"
          borderLeft
          raised
          isShow={showAlert}
          onClose={() => setShowAlert(false)}
          closable={true}
        >
          {messageResponse}
        </Alert>
      )}
      <SectionTitle title="" subtitle="Add Profile Employee" />
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        onSubmit={async (values, formikHelpers) => {
          onSubmit(values, formikHelpers);
        }}
      >
        {({ setFieldValue, values, errors, isSubmitting, handleChange }) => {
          return (
            <Form>
              <div className="flex flex-col gap-y-6">
                {/* data popcenter */}
                <div className="w-full p-4 pb-8 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800">
                  <p className="font-bold mb-6">Data Employee</p>

                  <div className="pictures grid gap-y-3 md:gap-y-0 md:grid-cols-8 md:gap-x-3">
                    {uploadedPictures?.map((picture, index) => {
                      return (
                        <div className="relative" key={index}>
                          <img
                            src={picture.url}
                            alt="popcenter"
                            className="w-full h-full object-contain rounded-lg max-h-full"
                          />
                          <div
                            className="absolute top-1 right-1"
                            onClick={() => handleRemoveImage(index)}
                          >
                            <img
                              className="cursor-pointer"
                              src="/images/icon-delete-circle.png"
                              alt="remove-picture"
                            />
                          </div>
                        </div>
                      );
                    })}
                    <div
                      className="relative cursor-pointer"
                      onClick={handleUploadImage}
                    >
                      <input
                        hidden
                        type="file"
                        name="images"
                        ref={refUpload}
                        accept="image/png, image/gif, image/jpeg"
                        onChange={handleOnSelectedPicture}
                      />
                      <img
                        className="w-full object-contain"
                        src="/images/border.png"
                        alt="border"
                      />
                      <div className="absolute top-0 w-full h-full flex justify-center items-center flex-col">
                        <img src="/images/icon-picture.png" alt="select-picture" />
                        <p>Tambahkan Foto</p>
                      </div>
                    </div>
                  </div>
                  {messageUploadPicture.length > 0 && (
                    <p className="text-xs text-red-500 mt-5">
                      {messageUploadPicture}
                    </p>
                  )}
                  <p className="text-xs text-gray-400 mt-5">
                    Foto dapat ditambahkan hingga 3 foto
                  </p>

                  <div className="flex flex-col md:flex-row gap-x-14">
                    <div className="md:w-1/2">
                      <div className="form-element">
                        <div className="form-label">Employe ID</div>
                        <Field
                          type="text"
                          className="form-input bg-gray-200 text-sm  border  border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                          name="uuidEmployee"
                          placeholder="Employe ID"
                          required={true}
                          value={values.uuidEmployee || ''}
                          readOnly
                        />
                      </div>
                    </div>
                    <div className="md:w-1/2">
                      <div className="form-element">
                        <div className="form-label">Nama</div>
                        <Field
                          type="text"
                          className="form-input text-sm  border  border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                          name="name"
                          placeholder="Masukkan Nama"
                          value={values.name || ''}
                          required={true}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="flex flex-col md:flex-row gap-x-14">
                    <div className="md:w-1/2">
                      <div className="form-element">
                        <div className="form-label">Email*</div>
                        <Field
                          type="text"
                          className="form-input text-sm  border  border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                          name="email"
                          placeholder="Masukkan Email"
                          value={values.email || ''}
                          required={true}
                        />
                      </div>
                    </div>
                    <div className="md:w-1/2">
                      <div className="form-element">
                        <div className="form-label">Position Job*</div>
                        <Field
                          type="text"
                          className="form-input text-sm  border  border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                          name="positionJob"
                          placeholder="Masukkan Position Job"
                          value={values.positionJob || ''}
                          required={true}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="flex flex-col md:flex-row gap-x-14">
                    <div className="md:w-1/2">
                      <div className="form-element">
                        <div className="form-label">Phone*</div>
                        <Field
                          type="text"
                          className="form-input text-sm  border  border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                          name="phone"
                          placeholder="Masukkan Phone"
                          value={values.phone || ''}
                          required={true}
                        />
                      </div>
                    </div>

                    <div className="md:w-1/2">
                      <div className="form-element">
                        <div className="form-label">Password</div>
                        <Field
                          type="text"
                          className="form-input text-sm  border  border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                          name="password"
                          placeholder="Masukkan Password"
                          value={values.password ||''}
                        />
                      </div>
                    </div>
                  </div>

                  {user?.data?.type === 'ADMINHRD' && (
                    <div className="flex flex-col md:flex-row gap-x-14">
                      <div className="md:w-1/2">
                        <div className="form-element">
                          <div className="form-label">Status*</div>
                          <Field
                            className="custom-select text-sm"
                            name="status"
                            options={[
                              { value: 'ACTIVE', label: 'Active' },
                              { value: 'INACTIVE', label: 'InActive' },
                            ]}
                            component={SelectReact}
                            placeholder=""
                            value={values.status || ''}
                            required={true}
                          />
                        </div>
                      </div>
                      <div className="md:w-1/2">
                        <div className="form-element">
                          <div className="form-label">Type*</div>
                          <Field
                            className="custom-select text-sm"
                            name="type"
                            options={[
                              { value: 'EMPLOYEE', label: 'Employee' },
                              { value: 'ADMINHRD', label: 'Admin / HRD' },
                            ]}
                            component={SelectReact}
                            placeholder=""
                            value={values.type || ''}
                            required={true}
                          />
                        </div>
                      </div>
                    </div>
                  )}
                </div>


                {/* button */}
                <div className="flex gap-x-3 justify-center">
                  <input
                    type="submit"
                    className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
                    value={isLoadingSubmit ? 'Loading...' : 'Simpan'}
                    disabled={isLoadingSubmit}
                  />

                  <Link href={'/employee'}>
                    <a className="md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded text-center">
                      Batal
                    </a>
                  </Link>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};
export default LocationAdd;
