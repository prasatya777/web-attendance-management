import dynamic from 'next/dynamic';
import {
  useConfigUser
} from '@data/employee/useEmployee';
import Alert from '@components/alerts';
import { useUser } from '@data/useUser';
import { configUserLevel } from '@utils/configUserLevel';
import { useState } from 'react';
import { clockIn } from '@services/attendance/attendanceService';
const ModalSuccessSync = dynamic(() =>
  import('@components/employee/modals/modalSuccess'),
);

const SectionTitle = dynamic(() => import('@components/dashboard/section-title'));

const ErrorPage = dynamic(() => import('@pages/401'));

const Attendance = () => {
  const { user, loading: loadingUser } = useUser({});

    const [isLoadingClockIn, setIsLoadingClockIn] = useState(false);
    const [isLoadingClockOut, setIsLoadingClockOut] = useState(false);
    const [isModalSuccessOpen, setIsModalSuccessOpen] = useState(false);
    const [isSuccessSync, setIsSuccessSync] = useState(false);
    const [messageResponse, setMessageResponse] = useState(null);

    const [successMessage, setSuccessMessage] = useState('Succes Attendance');
    const [failedMessage, setFailedMessage] = useState('Failed Attandance');
    const [showAlert, setShowAlert] = useState(true);

  // SECTION handler access level page
  const { data: isHaveAccess } = useConfigUser({
    userToken: user?.token,
    params: {
      nameConfig: configUserLevel.MENU_ATTENDANCE_EMPLOYEE,
    },
  });

  if (!loadingUser) {
    if (!isHaveAccess) {
      return <ErrorPage statusCode={401} />;
    }
  }

  const handlerClockIn = async () => {
    try {
        setIsLoadingClockIn(true);
        const resClockIn = await clockIn({ token: user?.token,
            payload: {
                uuidEmployee: user?.data?.uuidEmployee,
                type: 'CLOCKIN'
            }
        });

        if(resClockIn){
            setSuccessMessage('Succes Attendance Clock In')
            setIsSuccessSync(true)
        }else{
            setFailedMessage('Failed / Already Attendance Clock In')
            setIsSuccessSync(false)
        }

        setIsModalSuccessOpen(true);
      } catch (error) {
        setIsModalSuccessOpen(false);
        setIsSuccessSync(false);
        setMessageResponse('Failed / Already Attendance Clock In');
        setShowAlert(true)
      } finally {
        setIsLoadingClockIn(false);
      }
  };

    const handlerClockOut = async () => {
    try {
        setIsLoadingClockOut(true);

        const resClockOut = await clockIn({ token: user?.token, payload: {
            uuidEmployee: user?.data?.uuidEmployee,
            type: 'CLOCKOUT'
        } });

        if(resClockOut?.status){
            setSuccessMessage('Succes Attendance Clock Out')
            setIsSuccessSync(true)
        }else{
            setFailedMessage('Failed / Already Attendance Clock Out')
            setIsSuccessSync(false)
        }

        setIsModalSuccessOpen(true);
      } catch (error) {
        setIsModalSuccessOpen(false);
        setIsSuccessSync(false);
        setMessageResponse('Failed / Already Attendance Clock Out');
        setShowAlert(true)
      } finally {
        setIsLoadingClockOut(false);
      }
  };

  return (
    <>

        {messageResponse && (
            <Alert
            color="bg-transparent border-red-500 text-red-500"
            borderLeft
            raised
            isShow={showAlert}
            onClose={() => setShowAlert(false)}
            closable={true}
            >
            {messageResponse}
            </Alert>
        )}
      <SectionTitle title="" subtitle="Attendance" />

        <div className="grid lg:grid-cols-2 gap-x-3 gap-y-3 w-full mb-2 lg:mb-5">
            <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0 mb-3 h-full">
                <button className="flex h-20 w-full cursor-pointer rounded-lg bg-blue-500 text-white gap-x-3 items-center justify-center"
                    onClick={handlerClockIn}
                    value={isLoadingClockIn ? 'Loading...' : 'Clock In'}
                    disabled={isLoadingClockIn}
                >
                    <img src="/images/icon-user-plus.png" alt="add-user" />
                    <p>{isLoadingClockIn ? 'LOADING...' : 'Clock In'}</p>
                </button>
            </div>
        </div>

        <div className="grid lg:grid-cols-2 gap-x-3 gap-y-3 w-full mb-2 lg:mb-5">
            <div className="w-full lg:space-x-2 space-y-2 lg:space-y-0 mb-3 h-full">
                <button className="flex w-full h-20 cursor-pointer rounded-lg bg-red-500 text-white gap-x-3 items-center justify-center"
                    onClick={handlerClockOut}
                    value={isLoadingClockOut ? 'Loading...' : 'Clock In'}
                    disabled={isLoadingClockOut}
                >
                    <img src="/images/icon-user-plus.png" alt="add-user" />
                    <p>{isLoadingClockOut ? 'LOADING...' : 'Clock Out'}</p>
                </button>
            </div>
        </div>

    {isModalSuccessOpen && (
        <ModalSuccessSync
          isOpen={isModalSuccessOpen}
          onClose={() => setIsModalSuccessOpen(false)}
          shouldCloseOnOverlayClick={true}
          isSuccess={isSuccessSync}
          successMessage={successMessage}
          failedMessage={failedMessage}
        />
      )}
    </>
  );
};
export default Attendance;
