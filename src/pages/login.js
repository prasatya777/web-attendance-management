import Login from '@components/forms/Login';
const Index = () => {
  return (
    <>
      <div className="w-full flex flex-row h-screen overflow-hidden">
        <div className="hidden lg:flex lg:flex-col w-1/2 items-center justify-center relative">
          <img
            className="object-contain"
            src="/images/login-illustration.png"
            alt="svg"
          />
          <div className="absolute w-full h-full  flex justify-center mt-96">
            <div className="flex gap-x-2">
              <div
                className="text-right text-red-600"
                style={{ fontFamily: 'Impact' }}
              >
                <p style={{ lineHeight: '45px' }} className="lg:text-5xl ">
                  Attendance Management System Dashboard
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="w-full lg:w-1/2 bg-white p-8 lg:p-24 flex flex-col items-start justify-center">
          <p
            className="text-5xl font-medium mb-16 text-gray-300"
            style={{ fontFamily: 'Inter' }}
          >
            LOGIN
          </p>
          <Login />
        </div>
      </div>
    </>
  );
};

export default Index;
