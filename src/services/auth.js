import axios from 'axios';

export const login = (payload) => {
  return axios.post('/api/login', payload).then((res) => res.data);
};
export const logout = () => axios.post('/api/logout').then((res) => res.data);

export const registerUser = async (payload) => {
  return axios.post(`/api/register`, payload).then((res) => res.data);
};

export const updateUserProfile = (payload, token) =>
  axios.put('/me', payload, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export const forgotPassword = (email) =>
  axios.post('/auth/forget_password', { email });

export const resetPassword = (email, otp, password) =>
  axios.post('/auth/reset_password', {
    email,
    otp,
    password,
  });

export const changePassword = (payload) =>
  axios.post('/auth/change_password', payload);
