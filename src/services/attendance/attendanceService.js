import axios from '@configs/axiosAttendance';

export const clockIn = async ({ token, payload }) => {
  return axios
    .post(`${process.env.NEXT_PUBLIC_API_ATTENDANCE_URL}/attendance`, JSON.stringify(payload), {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
        'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_ATTENDANCE_HEADER_AUTH,
      },
      transformRequest: (data) => {
        return data;
      },
    })
    .then((res) =>  res);
};
