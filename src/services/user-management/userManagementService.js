import axios from '@configs/axios';

export const createUserManagement = async ({ token, payload }) => {
  return axios
    .post(`/user/addUser`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => res.data);
};

export const updateUserManagement = async ({ token, payload }) =>
  axios
    .put(`/user/updateUser`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      params: {
        id: payload.id,
      },
    })
    .then((res) => res.data);
