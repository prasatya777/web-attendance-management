import axios from '@configs/axios';
// import axios from 'axios';

export const getPopCenterID = async ({ token }) => {
  if (token) {
    return axios
      .get(`/popCenter/getCreate`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => res.data);
  }
};

export const createLocation = async ({ token, payload }) => {
  return axios
    .post(`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/add`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_EMPLOYEE_HEADER_AUTH,
      },
      transformRequest: (data) => {
        return data;
      },
    })
    .then((res) => res.data);
};

export const updateLocation = async ({ token, payload }) => {
  return axios
    .put(`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/update`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_EMPLOYEE_HEADER_AUTH,
      },
      transformRequest: (data) => {
        return data;
      },
    })
    .then((res) => res.data);
};

export const syncDataLocation = async ({ userToken, payload }) => {
  return axios
    .post(`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/syncPricePpc`, payload, {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    })
    .then((res) => res.data);
};

export const syncLockerLocation = async ({ userToken, payload }) => {
  return axios
    .post(
      `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/lockerLocation/sync`,
      payload,
      {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      },
    )
    .then((res) => res.data);
};
