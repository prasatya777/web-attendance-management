import axios from '@configs/axios';

export const addEmployee = async ({ token, payload }) => {
  return axios
    .post(`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/add`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_EMPLOYEE_HEADER_AUTH,
      },
      transformRequest: (data) => {
        return data;
      },
    })
    .then((res) => res.data);
};

export const updateEmployee = async ({ token, payload }) => {
  return axios
    .put(`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/update`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        'API-HEADER-AUTH': process.env.NEXT_PUBLIC_API_EMPLOYEE_HEADER_AUTH,
      },
      transformRequest: (data) => {
        return data;
      },
    })
    .then((res) => res.status);
};
