const getColorByStatus = (status) => {
  const availableStatus = {
    COMPLETE: 'bg-green-500 text-white',
    SUCCESS: 'bg-green-500 text-white',
    PAID: 'bg-green-500 text-white',

    OVERDUE: 'bg-red-500 text-white',
    IN_STORE: 'bg-blue-400 text-white',
    CANCEL: 'bg-yellow-300 text-white',
    EXPIRED: 'bg-black text-white',
  };

  return availableStatus[status] || 'bg-gray-200 text-gray-700';
};

export default getColorByStatus;
