export const formatDayIndonesia = (day) => {
  switch (day) {
    case 0:
      return 'Minggu';
    case 1:
      return 'Senin';
    case 2:
      return 'Selasa';
    case 3:
      return 'Rabu';
    case 4:
      return 'Kamis';
    case 5:
      return "Jum'at";
    case 6:
      return 'Sabtu';
    default:
      return 'invalid day';
  }
};

export const formatMonthIndonesia = (month) => {
  switch (month) {
    case 0:
      return 'Januari';
    case 1:
      return 'Februari';
    case 2:
      return 'Maret';
    case 3:
      return 'April';
    case 4:
      return 'Mei';
    case 5:
      return 'Juni';
    case 6:
      return 'Juli';
    case 7:
      return 'Agustus';
    case 8:
      return 'September';
    case 9:
      return 'Oktober';
    case 10:
      return 'November';
    case 11:
      return 'Desember';
    default:
      return 'invalid month';
  }
};

export const getCurrentDate = () => {
  const date = new Date();
  const tahun = date.getFullYear();
  const bulan = formatMonthIndonesia(date.getMonth());
  const tanggal = date.getDate();
  const hari = formatDayIndonesia(date.getDay());

  const format = `${hari}, ${tanggal} ${bulan} ${tahun}`;
  return format;
};

export const formatDate = (originalDate) => {
  const newDate = new Date(originalDate);
  let tanggal = newDate.getDate();
  let bulan = newDate.getMonth() + 1;
  const tahun = newDate.getFullYear();

  if (tanggal < 10) tanggal = `0${tanggal}`;
  if (bulan < 10) bulan = `0${bulan}`;

  return `${tahun}-${bulan}-${tanggal}`;
};

export const formatSimpleDate = (originalDate, seperator = '-') => {
  const newDate = new Date(originalDate);
  let tanggal = newDate.getDate();
  let bulan = newDate.getMonth() + 1;
  const tahun = newDate.getFullYear().toString().slice(-2);

  if (tanggal < 10) tanggal = `0${tanggal}`;
  if (bulan < 10) bulan = `0${bulan}`;

  return `${tahun}${seperator}${bulan}${seperator}${tanggal}`;
};

export const secondsToTime = (second) => {
  const h = Math.floor(second / 3600)
      .toString()
      .padStart(2, '0'),
    m = Math.floor((second % 3600) / 60)
      .toString()
      .padStart(2, '0'),
    s = Math.floor(second % 60)
      .toString()
      .padStart(2, '0');

  return `${h}:${m}:${s}`;
};

export const get1MonthAgo = () => {
  const startDate = new Date();
  startDate.setDate(1);
  const currentDate = new Date();

  return `${formatDate(startDate)} - ${formatDate(currentDate)}`;
};

export const getFullDateFromISO = (ISODate) => {
  const date = new Date(ISODate);
  const year = date.getFullYear();
  const month = formatMonthIndonesia(date.getMonth());
  const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

  return `${day} ${month} ${year} `;
};

export const getFullTimeFromISO = (ISODate) => {
  const date = new Date(ISODate);
  const hour = date.getHours();
  const minutes =
    date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  const seconds =
    date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

  return `${hour}:${minutes}:${seconds}`;
};

export const getFullDateTimeFromISO = (ISODate) => {
  return `${getFullDateFromISO(ISODate)} ${getFullTimeFromISO(ISODate)}`;
};
