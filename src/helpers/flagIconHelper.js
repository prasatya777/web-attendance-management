import Flags from 'country-flag-icons/react/3x2';

const getFlagIcon = ({ name }) => {
  name = name.toUpperCase();
  const IconFlag = Flags[name];

  if (!IconFlag) {
    // Return a default on
    return null;
  }
  return <IconFlag title={name} className="w-7 h-7 mr-3" />;
};

export default getFlagIcon;
