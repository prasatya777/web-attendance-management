const fs = require('fs')
export const createFolder = async (dirPath) => {
  try {
    const dirPathLog = [{ path: dirPath, chmod: 0o755 }]
    await Promise.all(
      dirPathLog.map(async (v) => {
        if (!fs.existsSync(v.path)) {
          fs.mkdirSync(v.path)
          fs.chmod(v.path, v.chmod, async () => {
            console.log(v.path + ' Change Access to : ' + v.chmod.toString())
          })
        } else {
          fs.chmod(v.path, v.chmod, () => {
            console.log(v.path + ' Change Access to : ' + v.chmod.toString())
          })
        }
      }),
    )
  } catch (error) {
    console.log(error)
  }
}
