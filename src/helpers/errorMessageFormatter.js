export const getErrorMessageResponse = (error) => {
  const errors = [];
  const title = error.message;

  if (Array.isArray(error.errors)) {
    error.errors?.map((err) => {
      for (const i in err) {
        const message = err[i][0];
        errors.push(message);
      }

      return errors;
    });

    return `${title}: ${errors.join(', ')}`;
  } else {
    return title;
  }
};
