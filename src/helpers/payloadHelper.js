export const formatFormData = (payload) => {
  const formData = new FormData();
  for (const key in payload) {
    if (payload[key] instanceof Array) {
      if (key.toLowerCase() === 'images') {
        payload[key].forEach((item) => {
          formData.append(`${key}`, item);
        });
      } else {
        formData.append(`${key}`, JSON.stringify(payload[key]));
      }
    } else {
      formData.append(key, payload[key]);
    }
  }

  return formData;
};

export const formatParams = (params) => {
  let queryString = '';
  for (const key in params) {
    if (Array.isArray(params[key])) {
      const objectArray = params[key];
      for (const index in objectArray) {
        queryString += `&${key}[]=${objectArray[index]}`;
      }
    } else if (typeof params[key] === 'object') {
      const object = params[key];
      if (object.startDate !== '' && object.endDate !== '') {
        queryString += `&${key}=${object.startDate} - ${object.endDate}`;
      } else {
        queryString += '';
      }
    } else {
      queryString += `&${key}=${params[key]}`;
    }
  }

  return queryString.substring(1);
};
