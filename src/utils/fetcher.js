import axios from 'axios';

export const fetcher = (...args) => {
  switch (args.length) {
    case 2:
      return axios
        .get(args[0], {
          headers: {
            authorization: 'Bearer ' + args[1],
            'api-header-auth': process.env.NEXT_PUBLIC_API_EMPLOYEE_HEADER_AUTH,
          },
        })
        .then(({ data }) => data.data);

    case 1:
      return axios(args[0]).then(({ data }) => data.data);

    default:
      return null;
  }
};
