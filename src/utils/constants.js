const listTower = [
  {
    value: 'CHA',
    label: 'CHA',
  },
  {
    value: 'CHB',
    label: 'CHB',
  },
  {
    value: 'MSA',
    label: 'MSA',
  },
  {
    value: 'MSB',
    label: 'MSB',
  },
  {
    value: 'OGA',
    label: 'OGA',
  },
  {
    value: 'OGB',
    label: 'OGB',
  },
  {
    value: 'Marketing Office',
    label: 'Marketing Office',
  },
  {
    value: 'BM',
    label: 'BM',
  },
]

const PAYMENT_STATUS = {
  PENDING: 'pending',
  DONE: 'done',
}

const LINK_DOWNLOAD_CSV = {
  inventory: '/api/generateInventoryCsv',
  outbound: '/api/generateOutboundCsv',
  popcenter: '/api/generatePopcenterCsv',
}

const VALIDATION_INPUT = {
  LOCATION_MIN_LENGTH: process.env.NEXT_PUBLIC_MIN_LENGTH_LOCATION ?? 4,
  PHONE_MAX_LENGTH: process.env.NEXT_PUBLIC_MAX_LENGTH_PHONE ?? 4,
  PHONE_MIN_LENGHT: process.env.NEXT_PUBLIC_MIN_LENGTH_PHONE ?? 4,
}

export { listTower, PAYMENT_STATUS, LINK_DOWNLOAD_CSV, VALIDATION_INPUT }
