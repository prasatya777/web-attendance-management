export const configUserLevel = {
  MENU_ATTENDANCE_EMPLOYEE: 'menuAttendanceEmployee',
  MENU_HISTORY_ATTENDANCE_EMPLOYEE: 'menuHistoryAttendanceEmployee',
  MENU_PROFILE_EMPLOYEE: 'menuProfileEmployee',

  MENU_EMPLOYEE_HR: 'menuEmployeeHr',
  MENU_DASHBOARD: 'menuDashboard',
};
