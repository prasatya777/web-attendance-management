import React, { useEffect, useRef } from 'react';
import Portal from '@components/portal';

const ModalError = ({
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  data = {
    headerMessage: '',
    textMessage: '',
  },
}) => {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (modalRef.current.contains(event.target) || !shouldCloseOnOverlayClick) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-5xl"
              ref={modalRef}
            >
              <div
                className="flex flex-col h-full justify-center rounded-2xl items-center bg-white"
                style={{ minHeight: 500, maxHeight: 500 }}
              >
                <img
                  className="mb-5"
                  width={125}
                  src="/images/ic-error.png"
                  alt="success"
                />
                <h3 className="font-bold text-xl">{data?.headerMessage}</h3>
                <h5 className="text-xs mt-3 text-gray-400">{data?.textMessage}</h5>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalError;
