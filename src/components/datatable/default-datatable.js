import React from 'react';
import { useTable, useSortBy, useRowSelect } from 'react-table';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';
import Loading from '@components/loading';

const DefaultDataTable = ({
  columns,
  data,
  isLoading,
  isSortAble = true,
  headerColor = 'bg-gray-200',
}) => {
  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, rows } =
    useTable(
      {
        columns,
        data,
      },
      useSortBy,
      useRowSelect,
    );

  // Render the UI for your table
  return (
    <>
      <div className="overflow-x-scroll">
        <table {...getTableProps()} className="table no-border">
          <thead className={[headerColor, ''].join(' ')} style={{ height: 50 }}>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps(
                      isSortAble ? column.getSortByToggleProps() : '',
                    )}
                  >
                    <div className="flex flex-row items-center justify-start whitespace-nowrap">
                      <span className="font-bold">{column.render('Header')}</span>
                      <span className="ml-auto">
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <FiChevronDown className="stroke-current text-2xs" />
                          ) : (
                            <FiChevronUp className="stroke-current text-2xs" />
                          )
                        ) : (
                          ''
                        )}
                      </span>
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {isLoading ? (
              <tr>
                <td
                  style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                  colSpan={99}
                >
                  <Loading text="" />
                </td>
              </tr>
            ) : rows.length > 0 ? (
              rows.map((row) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()} className="border-b">
                    {row.cells.map((cell) => (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    ))}
                  </tr>
                );
              })
            ) : (
              <tr>
                <td
                  style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                  colSpan={99}
                >
                  No Data
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default DefaultDataTable;
