import React, { useEffect } from 'react';
import { useTable, useSortBy, useRowSelect, useGlobalFilter } from 'react-table';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';
import Loading from '@components/loading';

const SearchDataTabless = ({
  columns,
  data,
  isLoading,
  isSortAble = true,
  headerColor = 'bg-gray-200',
  SearchComponent,
  showCheckbox = false,
  handlerAmbilBanyak = () => {},
  onRowSelectStateChange = () => {},
}) => {
  const CheckboxComponent = React.forwardRef(({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return (
      <input
        type="checkbox"
        ref={resolvedRef}
        {...rest}
        className="form-checkbox h-4 w-4"
      />
    );
  });

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    rows,
    state: { selectedRowIds, globalFilter },
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    useRowSelect,
    (hooks) => {
      showCheckbox &&
        hooks.visibleColumns.push((columns) => [
          // Let's make a column for selection
          {
            id: 'selection',
            // The header can use the table's getToggleAllRowsSelectedProps method
            // to render a checkbox
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <>
                <CheckboxComponent {...getToggleAllRowsSelectedProps()} />
              </>
            ),
            // The cell can use the individual row's getToggleRowSelectedProps method
            // to the render a checkbox
            Cell: ({ row }) => (
              <>
                <CheckboxComponent {...row.getToggleRowSelectedProps()} />
              </>
            ),
          },
          ...columns,
        ]);
    },
  );

  useEffect(() => {
    const x = data.filter((item, index) =>
      Object.keys(selectedRowIds)
        .map((i) => parseInt(i, 10))
        .includes(index),
    );
    onRowSelectStateChange?.(x);
  }, [onRowSelectStateChange, selectedRowIds]);

  // Render the UI for your table
  return (
    <>
      <SearchComponent
        filter={globalFilter}
        setFilter={setGlobalFilter}
        handlerAmbilBanyak={handlerAmbilBanyak}
      />
      <div className="overflow-auto max-h-128">
        <table {...getTableProps()} className="table no-border">
          <thead className={[headerColor, ''].join(' ')} style={{ height: 50 }}>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps(
                      isSortAble ? column.getSortByToggleProps() : '',
                    )}
                  >
                    <div className="flex flex-row items-center justify-start whitespace-nowrap">
                      <span className="font-bold">{column.render('Header')}</span>
                      <span className="ml-auto">
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <FiChevronDown className="stroke-current text-2xs" />
                          ) : (
                            <FiChevronUp className="stroke-current text-2xs" />
                          )
                        ) : (
                          ''
                        )}
                      </span>
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {isLoading ? (
              <tr>
                <td
                  style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                  colSpan={99}
                >
                  <Loading text="" />
                </td>
              </tr>
            ) : rows.length > 0 ? (
              rows.map((row) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()} className="border-b">
                    {row.cells.map((cell) => (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    ))}
                  </tr>
                );
              })
            ) : (
              <tr>
                <td
                  style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                  colSpan={99}
                >
                  No Data
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default SearchDataTabless;
