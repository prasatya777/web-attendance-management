import React, { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { FiChevronRight } from 'react-icons/fi';
import * as Icons from 'react-icons/fi';
import { useUser } from '@data/useUser';
import { useConfigUserLevel } from '@data/user-management/useUserManagement';
import { configUserLevel } from '@utils/configUserLevel';

const DynamicFiIcon = ({ name }) => {
  const IconComponent = Icons[name];

  if (!IconComponent) {
    // Return a default one
    return <Icons.FiCompass size={20} />;
  }
  return <IconComponent size={20} />;
};

const Item = ({ route, icon, name, badge, child = [], configLevelName = null }) => {
  const { user } = useUser({});
  const [hidden, setHidden] = useState(true);
  const router = useRouter();
  let { pathname } = { ...router };

  let active = pathname.substring(1).includes(route) ? true : false;

  const { data: isGrantedChangePayment } = useConfigUserLevel({
    userToken: user?.token,
    params: {
      nameConfig: configUserLevel[configLevelName],
    },
  });


  if (isGrantedChangePayment?.status === true) {
    if (child.length === 0 || child === false || child === '') {
      return (
        <Link href={route.charAt(0) !== '/' ? '/' + route : route}>
          <a className={`left-sidebar-item ${active ? 'active' : ''}`}>
            <DynamicFiIcon name={icon} />
            <span className="title">{name}</span>
            {badge && (
              <span className={`badge badge-circle badge-sm ${badge.color}`}>
                {badge.text}
              </span>
            )}
          </a>
        </Link>
      );
    }
    return (
      <button
        onClick={() => setHidden(!hidden)}
        className={`left-sidebar-item ${active ? 'active' : ''} ${
          hidden ? 'hidden-sibling' : 'open-sibling'
        }`}
      >
        <DynamicFiIcon name={icon} />
        <span className="title">{name}</span>
        {badge && (
          <span className={`badge badge-circle badge-sm ${badge.color}`}>
            {badge.text}
          </span>
        )}
        <FiChevronRight className="ml-auto arrow" />
      </button>
    );
  } else {
    return '';
  }
};

export default Item;
