import React from 'react';
import dynamic from 'next/dynamic';

const Logo = dynamic(() => import('./logo'));
const Title = dynamic(() => import('./title'));
const Item = dynamic(() => import('./item'));

const LeftSidebar = () => {
  const data = [
    {
      id: 59,
      name: 'Dashboard',
      route: '/',
      route_name: '/',
      icon: 'fas fa-box-open',
      child: [],
      configLevelName: 'MENU_DASHBOARD',
    },
    {
      id: 59,
      name: 'Attendance Employee',
      route: 'attandance',
      route_name: 'attandance',
      icon: 'fas fa-box-open',
      child: [],
      configLevelName: 'MENU_ATTENDANCE_EMPLOYEE',
    },
    {
      id: 59,
      name: 'Attendance History',
      route: 'attendance-history',
      route_name: 'attendance-history',
      icon: 'fas fa-box-open',
      child: [],

      configLevelName: 'MENU_HISTORY_ATTENDANCE_EMPLOYEE',
    },
    {
      id: 59,
      name: 'Profile',
      route: 'profile',
      route_name: 'profile',
      icon: 'fas fa-box-open',
      child: [],

      configLevelName: 'MENU_PROFILE_EMPLOYEE',
    },
    {
      id: 59,
      name: 'Employee Management',
      route: 'employee/list',
      route_name: 'employee/list',
      icon: 'fas fa-box-open',
      child: [],

      configLevelName: 'MENU_EMPLOYEE_HR',
    },
  ];

  return (
    <div className="left-sidebar left-sidebar-1">
      <Logo />
      <Title>MENU DASHBOARD</Title>
      <ul>
        {data.map((l0, a) => {
          return (
            <li key={a} className="l0">
              <Item {...l0} />
              <ul>
                {l0.child?.map((l1, b) => {
                  if (l1.child === '' || !l1.child) {
                    l1.child = [];
                  }
                  return (
                    <li key={b} className="l1">
                      <Item {...l1} />
                      <ul>
                        {l1.child?.map((l2, c) => {
                          if (l2.child === '' || !l2.child) {
                            l2.child = [];
                          }
                          return (
                            <li key={c} className="l2">
                              <Item {...l2} />
                              {/* <ul>
                                {l2.child?.map((l3, d) => (
                                  <li key={d} className="l3">
                                    <Item {...l3} />
                                    <ul>
                                      {l3.child?.map((l4, e) => (
                                        <li key={e} className="l4">
                                          <Item {...l4} />
                                        </li>
                                      ))}
                                    </ul>
                                  </li>
                                ))}
                              </ul> */}
                            </li>
                          );
                        })}
                      </ul>
                    </li>
                  );
                })}
              </ul>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default LeftSidebar;
