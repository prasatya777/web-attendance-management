import { FiMenu } from 'react-icons/fi';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import Link from 'next/link';

const Logo = () => {
  const dispatch = useDispatch();
  const { config, leftSidebar } = useSelector(
    (state) => ({
      config: state.config,
      leftSidebar: state.leftSidebar,
    }),
    shallowEqual,
  );
  const { collapsed } = { ...config };
  const { showLogo } = { ...leftSidebar };
  if (showLogo) {
    return (
      <div className="logo">
        <Link href="/">
          <span className="flex justify-center w-full">
            <a className="text-2xl w-full text-center font-normal normal-case flex justify-center">
              <div
                className="text-right text-red-600"
                style={{ fontFamily: 'Impact' }}
              >
                <p className="lg:text-lg" style={{ lineHeight: '20px' }}>
                  Attendance Management
                </p>
                <p className="text-sm">By Andika Prasatya</p>
              </div>
            </a>
          </span>
        </Link>
        <button
          onClick={() =>
            dispatch({
              type: 'SET_CONFIG_KEY',
              key: 'collapsed',
              value: !collapsed,
            })
          }
          className="ml-auto mr-4 block md:hidden"
        >
          <FiMenu size={20} />
        </button>
      </div>
    );
  }
  return null;
};

export default Logo;
