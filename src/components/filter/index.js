import React from 'react';
import FilterForm from '@components/forms/filter';
import { useUser } from '@data/useUser';
import { useFilter } from '@data/useFilter';

export default function Filter({ data, isLoading, onSubmit }) {
  const initialValues = {
    country_code: '',
    locker_id: [],
    video_play_date: '',
    building_type: '',
    group: '',
    developer_group: '',
    partner: '',
    ads_video: [],
  };

  const { user } = useUser({});
  const { data: filterData, loading: loadingFilter } = useFilter(user?.token);

  const onFilterClick = (values) => {
    onSubmit(values);
  };

  return (
    <div className="widget w-full p-8 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800">
      <div className="text-xs uppercase font-light text-gray-500">Filter</div>
      <div className="text-xl font-bold mt-3">
        <FilterForm
          initialValues={initialValues}
          onSubmit={onFilterClick}
          data={filterData}
          isLoading={loadingFilter}
        />
      </div>
    </div>
  );
}
