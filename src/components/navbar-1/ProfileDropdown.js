import React, { useState, useEffect, useRef } from 'react';
import AccountLinks from './account-links';
import { useUser } from '@data/useUser';

const ProfileDropdown = () => {
  const [hidden, setHidden] = useState(true);

  const buttonRef = useRef(null);
  const dropdownRef = useRef(null);

  const { user } = useUser({
    redirectTo: '/login',
  });

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        hidden ||
        buttonRef.current.contains(event.target) ||
        dropdownRef.current.contains(event.target)
      ) {
        return false;
      }
      setHidden(!hidden);
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [hidden, dropdownRef, buttonRef]);

  const handleDropdownClick = () => {
    setHidden(!hidden);
  };

  return (
    <div className="relative mx-4 lg:mx-8">
      <button
        ref={buttonRef}
        onClick={handleDropdownClick}
        className="flex h-16 w-24 md:w-48 rounded-full ml-2 relative items-center justify-end"
      >
        <img
          className="h-8 w-8 rounded-full shadow"
          src={`/images/faces/m1.png`}
          alt="avatar"
        />
        <p className="ml-2">{user?.data?.name ?? '-'}</p>
      </button>
      <div
        ref={dropdownRef}
        className={`dropdown absolute top-0 right-0 mt-16 ${hidden ? '' : 'open'}`}
      >
        <div className="dropdown-content w-48 bottom-end">
          <AccountLinks />
        </div>
      </div>
    </div>
  );
};

export default ProfileDropdown;
