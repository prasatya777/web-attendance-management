import Link from 'next/link';
import { FiLogIn } from 'react-icons/fi';
import { logout } from '@services/auth';
import { useUser } from '@data/useUser';

const AccountLinks = () => {
  const { mutate: userMutate } = useUser({});

  const handlerLogout = async () => {
    await logout();
    userMutate();
  };

  const items = [
    {
      action: handlerLogout,
      icon: <FiLogIn size={18} className="stroke-current" />,
      name: 'Logout',
      badge: null,
    },
  ];

  return (
    <div className="flex flex-col w-full">
      <ul className="list-none">
        {items.map((item, i) => (
          <li key={i} className="dropdown-item">
            {item.action && (
              <button
                className="flex flex-row items-center justify-start h-10 w-full px-2"
                onClick={item.action}
              >
                {item.icon}
                <span className="mx-2">{item.name}</span>
                {item.badge && (
                  <span
                    className={`uppercase font-bold text-center p-0 leading-none text-2xs h-4 w-4 inline-flex items-center justify-center rounded-full ${item.badge.color} ml-auto`}
                  >
                    {item.badge.number}
                  </span>
                )}
              </button>
            )}

            {!item.action && (
              <Link href={item.url}>
                <a className="flex flex-row items-center justify-start h-10 w-full px-2">
                  {item.icon}
                  <span className="mx-2">{item.name}</span>
                  {item.badge && (
                    <span
                      className={`uppercase font-bold text-center p-0 leading-none text-2xs h-4 w-4 inline-flex items-center justify-center rounded-full ${item.badge.color} ml-auto`}
                    >
                      {item.badge.number}
                    </span>
                  )}
                </a>
              </Link>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default AccountLinks;
