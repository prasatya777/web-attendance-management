
import ProfileDropdown from './ProfileDropdown';

const Navbar = () => {

  const ENVIRONTMENT = process.env.NEXT_PUBLIC_ENVIRONTMENT;

  const getColorByEnvironment = () => {
    switch (ENVIRONTMENT) {
      case 'development':
        return 'bg-green-200';
      case 'staging':
        return 'bg-yellow-200';
      default:
        return 'bg-gray-200';
    }
  };

  return (
    <div className="navbar navbar-1 border-b sticky top-0 " style={{ zIndex: 9 }}>
      <div className="navbar-inner w-full flex items-center justify-start">
        <span className="ml-auto"></span>

        {ENVIRONTMENT !== 'production' && (
          <div
            className={`hidden lg:block px-10 text-gray-300 rounded-md py-3 tracking-widest ${getColorByEnvironment()}`}
          >
            {ENVIRONTMENT}
          </div>
        )}

        <ProfileDropdown />
      </div>
    </div>
  );
};

export default Navbar;
