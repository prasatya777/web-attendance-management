import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';

export default function Pagination({
  isShow = true,
  handlerPageChange,
  totalAllData = 0,
  itemsPerPage = 10,
}) {
  const [totalPage, setTotalPage] = useState(0);

  useEffect(() => {
    setTotalPage(Math.ceil(totalAllData / itemsPerPage));
  }, [totalPage, totalAllData, itemsPerPage]);

  return (
    isShow && (
      <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        onPageChange={handlerPageChange}
        pageRangeDisplayed={5}
        pageCount={totalPage}
        previousLabel="< previous"
        renderOnZeroPageCount={null}
        activeClassName={'font-bold bg-gray-200'}
        containerClassName={'pagination'}
        disabledClassName={'bg-gray-600 text-white'}
      />
    )
  );
}
