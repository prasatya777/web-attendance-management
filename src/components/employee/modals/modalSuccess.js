import React, { useEffect, useRef } from 'react';
import Portal from '@components/portal';

const ModalSuccessSync = ({
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  isSuccess = true,
  successMessage = '',
  failedMessage = '',
}) => {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (modalRef.current.contains(event.target) || !shouldCloseOnOverlayClick) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-lg"
              ref={modalRef}
            >
              <div
                className="flex flex-col h-full justify-center rounded-2xl items-center bg-white"
                style={{ minHeight: 250, maxHeight: 250 }}
              >
                {isSuccess ? (
                  <>
                    <img
                      className="mb-5"
                      src="/images/icon-check-circle.png"
                      alt="success"
                    />
                    <p className="text-lg">{successMessage}</p>
                  </>
                ) : (
                  <>
                    <img className="mb-5" src="/images/ic-error.png" alt="failed" />
                    <p className="text-lg">{failedMessage}</p>
                  </>
                )}
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalSuccessSync;
