import React, { useEffect, useRef } from 'react';
import Portal from '@components/portal';
import { useState } from 'react';
const ModalPictures = ({
  initialValues,
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  onSubmit,
  onReset,
  data,
}) => {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (modalRef.current.contains(event.target) || !shouldCloseOnOverlayClick) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  const [selectedIndex, setSelectedIndex] = useState(0);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-lg"
              ref={modalRef}
            >
              <div
                className="bg-white rounded-lg"
                style={{ minHeight: 600, maxHeight: 600 }}
              >
                <div className="relative p-4 flex-auto modal-body">
                  <img
                    src={
                      data?.images
                        ? data?.images[selectedIndex]
                        : '/images/location-popcenter-1.png'
                    }
                    className="object-cover w-full max-w-full rounded-lg min-h-1/2 max-h-1/2"
                    alt="image"
                  />

                  <div className="grid grid-cols-6 mt-3 gap-x-3">
                    {data?.images?.map((image, i) => {
                      return (
                        image && (
                          <img
                            key={i}
                            src={image}
                            onClick={() => {
                              setSelectedIndex(i);
                            }}
                            className={`object-cover w-full max-w-full rounded-lg p-1 cursor-pointer ${
                              i === selectedIndex ? 'border-2 border-blue-500' : ''
                            }`}
                            alt="image"
                          />
                        )
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalPictures;
