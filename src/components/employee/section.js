const Section = ({ title, description, right = null, children, classname = '' }) => {
  return (
    <div
      className={[
        'w-full p-4 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800',
        classname,
      ].join(' ')}
    >
      <div className="flex flex-row items-center justify-between ">
        <div className="flex flex-col">
          <div className="text-xs font-light text-gray-500 mb-1">{title}</div>
          <div className="text-xl font-bold">{description}</div>
        </div>
        {right}
      </div>
      {children}
    </div>
  );
};

export default Section;
