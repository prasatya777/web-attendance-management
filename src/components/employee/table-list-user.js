import React from 'react';
import DefaultDatatable from '@components/datatable/default-datatable';

import Link from 'next/link';

const ListEmployee = ({ data, isLoading }) => {
  const columns = React.useMemo(
    () => {
      if(data){
        return [
          {
            Header: 'NAMA',
            accessor: 'name',
            Cell: ({ value }) => value,
          },
          {
            Header: 'NO. HANDPHONE',
            accessor: 'phone',
            Cell: ({ value }) => value,
          },
          {
            Header: 'EMAIL',
            accessor: 'email',
            Cell: ({ value }) => value,
          },
          {
            Header: 'Position',
            accessor: 'positionJob',
            Cell: ({ value }) => value,
          },
          {
            Header: 'Type Access',
            accessor: 'type',
            Cell: ({ value }) => value,
          },
          {
            Header: 'STATUS',
            accessor: 'status',
            Cell: ({ value }) => {
              switch (value.toLowerCase()) {
                case 'active':
                  return <span className="text-green-500">{value}</span>;
                case 'inactive':
                  return <span className="text-gray-500">{value}</span>;
                default:
                  return value;
              }
            },
          },
          {
            Header: 'Action',
            accessor: 'id',
            Cell: ({ row }) => {
              const { uuidEmployee } = row.original;
              return (
                <Link href={`/employee/edit/${uuidEmployee}`}>
                  <a className="text-blue-500">EDIT</a>
                </Link>
              );
            },
          },
        ]
      }

      return []
    }
  );

    return <DefaultDatatable columns={columns} data={data} isLoading={isLoading} />;
};

export default ListEmployee;
