import React from 'react';
import DefaultDatatable from '@components/datatable/default-datatable';

import data from '@json/dailyreport-list.json';
import { formatRupiah } from '@helpers/formatRupiah';

const TableDailyReport = () => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'DATE',
        accessor: 'date',
        Cell: ({ value }) => value,
      },
      {
        Header: 'LOCATION',
        accessor: 'location',
        Cell: ({ value }) => value,
      },
      {
        Header: 'INBOUND',
        accessor: 'inbound',
        Cell: ({ value }) => value,
      },
      {
        Header: 'OUTBOUND',
        accessor: 'outbound',
        Cell: ({ value }) => value,
      },
      {
        Header: 'TOTAL PAKET',
        accessor: 'totalPacket',
        Cell: ({ value }) => value,
      },
      {
        Header: 'S',
        accessor: 'size.s',
        Cell: ({ value }) => value,
      },
      {
        Header: 'M',
        accessor: 'size.m',
        Cell: ({ value }) => value,
      },
      {
        Header: 'L',
        accessor: 'size.l',
        Cell: ({ value }) => value,
      },
      {
        Header: 'XL',
        accessor: 'size.xl',
        Cell: ({ value }) => value,
      },
      {
        Header: 'INCOME',
        accessor: 'income',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
    ],
    []
  );
  const items = React.useMemo(() => data, []);
  return <DefaultDatatable columns={columns} data={items} />;
};

export default TableDailyReport;
