import React from 'react';
import { Form, Formik, Field } from 'formik';
import { SelectReact } from '@components/forms/Input';
import Loading from '@components/loading';
import { useUser } from '@data/useUser';
import { useFilterListLocation } from '@data/locations/useLocations';

function FilterForm({
  initialValues,
  onSubmit = (values, formikHelpers) => {},
  onReset,
  isLoading = false,
}) {
  const { user } = useUser({});
  const { data: dataFilterList } = useFilterListLocation({ userToken: user?.token });

  if (isLoading) return <Loading text="please wait..." />;

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async (values, formikHelpers) => {
        onSubmit(values, formikHelpers);
      }}
      onReset={onReset}
    >
      {({ values, errors, isSubmitting, handleChange }) => (
        <Form>
          <div className="grid grid-cols-3 gap-x-3">
            <div className="form-element">
              <div className="form-label text-3xs">Nama PopCenter</div>
              <Field
                type="text"
                className="form-input text-sm  border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                name="name"
                placeholder="Masukkan Nama PopCenter"
              />
            </div>
            <div className="form-element">
              <div className="form-label text-3xs">PopCenter ID</div>
              <Field
                type="text"
                className="form-input text-sm  border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                name="uuidPopCenter"
                placeholder="Masukkan PopCenter ID"
              />
            </div>
            <div className="form-element">
              <div className="form-label text-3xs">Status</div>
              <Field
                className="custom-select text-sm"
                name="status"
                options={dataFilterList?.status ?? []}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
            <div className="form-element">
              <div className="form-label text-3xs">PIC</div>
              <Field
                className="custom-select text-sm"
                name="picId"
                options={dataFilterList?.picUser ?? []}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
            <div className="form-element">
              <div className="form-label text-3xs">Kota/Kab.</div>
              <Field
                className="custom-select text-sm"
                name="cityId"
                options={dataFilterList?.collectCity ?? []}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
            <div className="form-element">
              <div className="form-label text-3xs">Loker</div>
              <Field
                className="custom-select text-sm"
                name="lockerPopCenterId"
                options={dataFilterList?.lockers ?? []}
                component={SelectReact}
                placeholder=""
                isMulti={true}
              />
            </div>
          </div>

          <hr className="mb-5" />

          <div className="flex gap-x-3 justify-center">
            <input
              type="submit"
              className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
              value="Filter"
            />

            <input
              type="reset"
              className="md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded"
              value="Reset"
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}
export default FilterForm;
