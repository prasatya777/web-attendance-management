import React from 'react';
import Link from 'next/link';
// import faker from 'faker/locale/id_ID';

import DefaultDatatable from '@components/datatable/default-datatable';

const getColorByStatus = (status) => {
  switch (status.toLowerCase()) {
    case 'active':
      return 'text-green-500';
    case 'inactive':
      return 'text-red-500';
    case 'draft':
      return 'text-gray-500';
    default:
      return 'text-black';
  }
};

const ListLocationPopCenter = ({ data, isLoading }) => {
  // const DATAS = Array.from(Array(5).keys()).map((i) => ({
  //   id: i + 1,
  //   name: `PopCenter ${faker.address.city()}`,
  //   address: faker.address.streetAddress(),
  //   status: faker.random.arrayElement(['active', 'inactive', 'draft']),
  //   locker: faker.finance.amount(1, 10, 0),
  //   city: faker.address.cityName(),
  //   ops: 'Mo - Su 08:00-21:00',
  // }));
  const columns = React.useMemo(
    () => [
      {
        Header: 'NAMA',
        accessor: 'name',
        Cell: ({ row }) => {
          const { uuidPopCenter, name } = row.original;
          return (
            <Link href={`/locations/detail/${uuidPopCenter}`}>
              <a className="text-blue-500">{name}</a>
            </Link>
          );
        },
      },
      {
        Header: 'ALAMAT',
        accessor: 'address',
        Cell: ({ value }) => <p className="whitespace-pre-line">{value}</p>,
      },
      {
        Header: 'STATUS',
        accessor: 'status',
        Cell: ({ value }) => {
          return <span className={getColorByStatus(value)}>{value}</span>;
        },
      },
      {
        Header: 'LOKER',
        accessor: 'loker',
        Cell: ({ value }) => value,
      },
      {
        Header: 'KOTA/KAB',
        accessor: 'cityDetail',
        Cell: ({ row }) => {
          return row.original.cityDetail.map((city) => (
            <div key={city.cityId}>{city.cityName}</div>
          ));
        },
      },
      {
        Header: 'PIC',
        accessor: 'pic',
        Cell: ({ value }) => value,
      },
    ],
    [],
  );

  return <DefaultDatatable columns={columns} data={data} isLoading={isLoading} />;
};

export default ListLocationPopCenter;
