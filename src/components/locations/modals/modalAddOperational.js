import React, { useEffect, useRef } from 'react';
import Portal from '@components/portal';
import { FiX } from 'react-icons/fi';
import { Field, Form, Formik } from 'formik';
const ModalAddOperational = ({
  initialValues,
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  onSubmit,
  onReset,
}) => {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (modalRef.current.contains(event.target) || !shouldCloseOnOverlayClick) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-3xl"
              ref={modalRef}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <div>
                    <h2 className="text-xs text-gray-400">BUAT POPCENTER BARU</h2>
                    <h3 className="text-xl font-bold">JAM OPERASIONAL</h3>
                  </div>
                  <button
                    className="modal-close btn btn-transparent"
                    onClick={onClose}
                  >
                    <FiX size={18} className="stroke-current" />
                  </button>
                </div>

                <div className="relative p-4 flex-auto modal-body">
                  <Formik
                    enableReinitialize={true}
                    initialValues={initialValues}
                    onSubmit={async (values, formikHelpers) => {
                      onSubmit(values);
                    }}
                    onReset={onReset}
                  >
                    {({ values, errors, isSubmitting, handleChange }) => {
                      return (
                        <Form>
                          <div className="overflow-x-scroll">
                            <table className="w-full">
                              <thead>
                                <tr className="text-left">
                                  <th>Buka</th>
                                  <th className="px-3">Hari</th>
                                  <th className="px-3">Jam Buka</th>
                                  <th className="px-3">Jam Tutup</th>
                                </tr>
                              </thead>

                              <tbody>
                                {initialValues?.map((_, i) => (
                                  <tr key={i}>
                                    <td className="">
                                      <Field
                                        className="form-input text-sm border border-gray-400 focus:ring-transparent rounded p-2 lg:p-5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                                        type="checkbox"
                                        name={`[${i}].isOpen`}
                                        checked={values[i].isOpen}
                                      />
                                    </td>
                                    <td className="p-3">
                                      <Field
                                        className={`
                                      ${values[i].isOpen ? '' : 'bg-gray-100'}
                                      
                                      form-input text-sm border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none`}
                                        type="text"
                                        name={`[${i}].day`}
                                        readOnly
                                      />
                                    </td>
                                    <td className="p-3">
                                      <Field
                                        className={`
                                       ${values[i].isOpen ? '' : 'bg-gray-100'}
                                       form-input text-sm border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none`}
                                        type="time"
                                        name={`[${i}].open`}
                                        readOnly={!values[i].isOpen}
                                      />
                                    </td>
                                    <td className="p-3">
                                      <Field
                                        className={`
                                      ${values[i].isOpen ? '' : 'bg-gray-100'}
                                      form-input text-sm border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none`}
                                        type="time"
                                        name={`[${i}].close`}
                                        readOnly={!values[i].isOpen}
                                      />
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </table>
                          </div>

                          <hr className="mb-5" />

                          <div className="flex gap-x-3 justify-center">
                            <input
                              type="submit"
                              className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
                              value="Simpan"
                            />

                            <button
                              className="md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded"
                              onClick={() => onClose()}
                            >
                              Batal
                            </button>
                          </div>
                        </Form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalAddOperational;
