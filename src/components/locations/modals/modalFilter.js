import React, { useEffect, useRef } from 'react';
import Portal from '@components/portal';
import { FiX } from 'react-icons/fi';
import { useUser } from '@data/useUser';
import FilterForm from '@components/locations/forms/filterForm';
const ModalFilter = ({
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  onSubmit,
  initialValues,
  onReset,
}) => {
  const { user } = useUser({});

  const onFilterClick = (values) => {
    onSubmit(values);
  };

  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (modalRef.current.contains(event.target) || !shouldCloseOnOverlayClick) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-5xl"
              ref={modalRef}
            >
              <div className="modal-content min-h-full">
                <div className="modal-header">
                  <div className="">
                    <h2 className="text-xs text-gray-400">LOKASI</h2>
                    <h3 className="text-xl font-bold">FILTER</h3>
                  </div>
                  <button
                    className="modal-close btn btn-transparent"
                    onClick={onClose}
                  >
                    <FiX size={18} className="stroke-current" />
                  </button>
                </div>

                <div className="relative p-4 flex-auto modal-body">
                  <FilterForm
                    initialValues={initialValues}
                    onSubmit={onFilterClick}
                    onReset={onReset}
                    user={user ?? null}
                  />
                </div>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalFilter;
