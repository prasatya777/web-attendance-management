import { useGetInboundType } from '@data/inbound/useInbound';
import { useUser } from '@data/useUser';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';

export default function IncomingPacket({ onSubmit }) {
  const { user } = useUser({});
  const initialValues = {
    inboundType: 'NEW_PACKAGE',
    inboundLabel: 'Paket Baru',
    awb: '',
  };

  const IncomingPacketSchema = Yup.object().shape({
    inboundType: Yup.string().required('Harap Pilih Tipe Paket'),
    awb: Yup.string().when('inboundType ', {
      is: 'LOCKER_OVERDUE_PACKAGE',
      then: Yup.string().required('AWB harap diisi'),
      otherwise: Yup.string().notRequired(),
    }),
  });

  const { data: listInboundType } = useGetInboundType({ userToken: user?.token });

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={async (values, formikHelpers) => {
        onSubmit(values, formikHelpers);
      }}
      validationSchema={IncomingPacketSchema}
    >
      {({
        setFieldValue,
        values,
        errors,
        isSubmitting,
        handleChange,
        resetForm,
      }) => {
        return (
          <Form className="form flex flex-wrap w-full h-full">
            <div className="w-full rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800">
              <div className="flex px-4 pt-4 flex-row items-center justify-between mb-2">
                <div className="flex flex-col">
                  <div className="font-light text-gray-500">
                    <div className="flex items-center justify-start space-x-6">
                      {listInboundType?.inboundType?.map((item, i) => {
                        return (
                          <label htmlFor={item?.value} key={i}>
                            <Field
                              id={item?.value}
                              type="radio"
                              name="inboundType"
                              value={item?.value}
                              className="mr-2"
                              onChange={(e) => {
                                const { value } = e.target;
                                setFieldValue('inboundType', value);
                                setFieldValue('inboundLabel', item?.label);
                              }}
                            />
                            {item?.label}
                          </label>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>

              <div className="content px-4 pb-5 flex w-full gap-x-3">
                <div className="form-element w-2/3">
                  <Field
                    id="awb"
                    type="text"
                    className={`form-input text-sm border uppercase border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none form-input
                    
                    ${errors['awb'] && 'border-red-500'}
                    
                    `}
                    name="awb"
                    placeholder="Ketik / Scan No. Resi"
                  />

                  {errors['awb'] && (
                    <div className="text-red-500 mt-2">
                      <ErrorMessage name={'awb'} />
                    </div>
                  )}
                </div>
                <div className="w-1/3">
                  <input
                    type="submit"
                    value="Lanjut"
                    className="btn btn-default w-full bg-blue-500 hover:bg-blue-600 text-white btn-rounded"
                  />
                </div>
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}
