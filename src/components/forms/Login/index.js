import React, { useState } from 'react';
import Alert from '@components/alerts';
import { useUser } from '@data/useUser';
import { login } from '@services/auth';
import router from 'next/router';
import { ErrorMessage, Field, Form, Formik } from 'formik';

const Login = () => {
  const [message, setMessage] = useState(null);
  const { mutate } = useUser({});

  const [isLoading, setIsLoading] = useState(false);

  const onSubmit = async (formData) => {
    try {
      setIsLoading(true);

      await mutate(await login(formData));
      router.push('/');
    } catch (error) {
      const message = error.response?.data?.message;
      setMessage(message);

    } finally {
      setIsLoading(false);
    }
  };

  const initialValues = {
    email: '',
    password: '',
  };

  return (
    <>
      <div className="flex flex-col w-full">
        {message && (
          <div className="w-full mb-4">
            <Alert
              color="bg-transparent border-red-500 text-red-500"
              borderLeft
              raised
            >
              {message}
            </Alert>
          </div>
        )}

        <Formik
          initialValues={initialValues}
          onSubmit={async (values, formikHelpers) => {
            onSubmit(values, formikHelpers);
          }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = 'Email is required';
            }
            if (!values.password) {
              errors.password = 'Password is required';
            }
            return errors;
          }}
        >
          {({ values, errors, isSubmitting, handleChange }) => {
            return (
              <Form className="form w-full">
                <div className="form-element">
                  <div className="form-label">Email</div>
                  <Field
                    className={`form-input text-sm  border border-gray-400 focus:ring-transparent w-full px-3 py-5 focus focus:border-gray-600 focus:outline-none active:outline-none rounded-xl ${
                      errors['email'] ? 'border-red-500' : ''
                    }`}
                    placeholder="enter your email"
                    name="email"
                    type="email"
                    required
                  />
                  {errors['email'] && (
                    <div className="form-error">
                      <ErrorMessage name={'email'} />
                    </div>
                  )}
                </div>

                <div className="form-element">
                  <div className="form-label">Password</div>
                  <Field
                    className={`form-input text-sm  border border-gray-400 focus:ring-transparent w-full px-3 py-5 focus focus:border-gray-600 focus:outline-none active:outline-none rounded-xl ${
                      errors['password'] ? 'border-red-500' : ''
                    }`}
                    placeholder="enter your password"
                    name="password"
                    type="password"
                    required
                  />
                  {errors['password'] && (
                    <div className="form-error">
                      <ErrorMessage name={'password'} />
                    </div>
                  )}
                </div>

                <div className="form-element">
                </div>

                <input
                  type="submit"
                  className="btn btn-default bg-blue-500 hover:bg-blue-600 text-white btn-rounded"
                  value={isLoading ? 'please wait...' : 'login'}
                  disabled={isLoading}
                />
              </Form>
            );
          }}
        </Formik>
      </div>
    </>
  );
};

export default Login;
