import React from 'react';
import { Form, Formik, Field } from 'formik';
import { SelectReact, DateRangeReact } from '@components/forms/Input';
import { useState } from 'react';
import { useEffect } from 'react';
import Loading from '@components/loading';

function FilterForm({
  initialValues,
  onSubmit = (values, formikHelpers) => {},
  onReset,
  isLoading = false,
  data,
  user,
}) {
  const [lockerOptions, setLockerOptions] = useState([]);
  const [groupOptions, setGroupOptions] = useState([]);
  const [buildingOptions, setBuildingOptions] = useState([]);
  const [partnershipCompanyOptions, setPartnershipCompanyOptions] = useState(
    []
  );
  const [videoAdsOptions, setVideoAdsOptions] = useState([]);

  useEffect(() => {
    if (data) {
      setGroupOptions([{ value: '', label: '' }, ...data.group]);
      setLockerOptions(data.locker);
      setBuildingOptions([{ value: '', label: '' }, ...data.building_type]);
      setPartnershipCompanyOptions([
        { value: '', label: '' },
        ...data.partnership_company,
      ]);
      setVideoAdsOptions(data.video_ads);
    }
  }, [data]);

  const countryOptions = [
    {
      value: '',
      label: 'All Country',
    },
    {
      value: 'ID',
      label: 'Indonesia',
      country_code: 'ID',
    },
    {
      value: 'MY',
      label: 'Malaysia',
      country_code: 'MY',
    },
  ];

  if (isLoading) return <Loading text="please wait..." />;

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async (values, formikHelpers) => {
        onSubmit(values, formikHelpers);
      }}
      onReset={onReset}
    >
      {({ values, errors, isSubmitting, handleChange }) => (
        <Form>
          <div className="flex flex-col md:flex-row -mx-3">
            <div className="md:w-1/2 px-3">
              <div className={`form-element`}>
                <div className="form-label text-3xs">Video Title</div>
                <Field
                  className="custom-select text-sm"
                  name="ads_video"
                  options={videoAdsOptions}
                  component={SelectReact}
                  placeholder=""
                  isMulti={true}
                />
              </div>
            </div>
            <div className="md:w-1/2 px-3">
              <div className={`form-element`}>
                <div className="form-label text-3xs">Range Date</div>
                <Field
                  className="form-input text-sm"
                  name="video_play_date"
                  component={DateRangeReact}
                  placeholder="Select Locker"
                  startDate={null}
                  endDate={null}
                />
              </div>
            </div>
          </div>
          <hr className="mb-5" />

          <div className="grid gap-3 grid-cols-1 md:grid-cols-3">
            {user.data?.partner_type === 'INTERNAL' && (
              <div className={`form-element`}>
                <div className="form-label text-3xs">Country</div>
                <Field
                  className="custom-select text-sm"
                  name="country_code"
                  options={countryOptions}
                  component={SelectReact}
                  placeholder=""
                  isMulti={false}
                />
              </div>
            )}

            <div className={`form-element`}>
              <div className="form-label text-3xs">Building Type</div>
              <Field
                className="custom-select text-sm"
                name="building_type"
                options={buildingOptions}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
            <div className={`form-element`}>
              <div className="form-label text-3xs">Locker Name</div>
              <Field
                className="custom-select text-sm"
                name="locker_id"
                options={lockerOptions ?? []}
                component={SelectReact}
                placeholder=""
                isMulti={true}
              />
            </div>
            <div className={`form-element`}>
              <div className="form-label text-3xs">Group</div>
              <Field
                className="custom-select text-sm"
                name="group"
                options={groupOptions}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>

            {user.data?.partner_type === 'INTERNAL' && (
              <div className={`form-element`}>
                <div className="form-label text-3xs">Partner</div>
                <Field
                  className="custom-select text-sm"
                  name="partner"
                  options={partnershipCompanyOptions}
                  component={SelectReact}
                  placeholder=""
                  isMulti={false}
                />
              </div>
            )}

            <div className={`form-element`}>
              <div className="form-label text-3xs">Developer Group</div>
              <Field
                className="custom-select text-sm"
                name="developer_group"
                options={[]}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
          </div>

          <div className="block text-right">
            <input
              type="reset"
              className="mr-4 md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded"
              value="Reset"
            />

            <input
              type="submit"
              className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
              value="Filter"
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}
export default FilterForm;
