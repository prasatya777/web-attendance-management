import React from 'react';
import { Form, Formik, Field } from 'formik';
import { SelectReact, DateRangeReact } from '@components/forms/Input';
import { useState } from 'react';
import { useEffect } from 'react';
import Loading from '@components/loading';

function FilterForm({
  initialValues,
  onSubmit = (values, formikHelpers) => {},
  onReset,
  isLoading = false,
  data,
  user,
}) {
  const [locations, setLocations] = useState([]);
  const [phones, setPhones] = useState([]);
  const [users, setUsers] = useState([]);
  useEffect(() => {
    if (data) {
      setLocations([{ value: '', label: '' }, ...data.lockers]);
      setPhones([{ value: '', label: '' }, ...data.phones]);
      setUsers([{ value: '', label: '' }, ...data.users]);
    }
  }, [data]);
  if (isLoading) return <Loading text="please wait..." />;

  const MyInput = ({ field, form, ...props }) => {
    return <input {...field} {...props} />;
  };
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async (values, formikHelpers) => {
        onSubmit(values, formikHelpers);
      }}
      onReset={onReset}
    >
      {({ values, errors, isSubmitting, handleChange }) => (
        <Form>
          <div className="flex flex-col md:flex-row -mx-3">
            <div className="md:w-1/2 px-3">
              <div className={`form-element`}>
                <div className="form-label text-3xs">Invoice ID</div>

                <Field
                  type="text"
                  className="form-input text-sm  border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                  name="invoice_id"
                  placeholder="invoice ID"
                  component={MyInput}
                />
              </div>
            </div>

            <div className="md:w-1/2 px-3">
              <div className={`form-element`}>
                <div className="form-label text-3xs">Transaction Date</div>
                <Field
                  className="form-input text-sm border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none "
                  name="transaction_date"
                  component={DateRangeReact}
                  startDate={null}
                  endDate={null}
                />
              </div>
            </div>
          </div>
          <div className="grid gap-3 grid-cols-1 md:grid-cols-3">
            <div className={`form-element`}>
              <div className="form-label text-3xs">Locations</div>
              <Field
                className="custom-select text-sm"
                name="location"
                options={locations}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
            <div className={`form-element`}>
              <div className="form-label text-3xs">User</div>
              <Field
                className="custom-select text-sm"
                name="user"
                options={users}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>
            <div className={`form-element`}>
              <div className="form-label text-3xs">Phone</div>
              <Field
                className="custom-select text-sm"
                name="phone"
                options={phones}
                component={SelectReact}
                placeholder=""
                isMulti={false}
              />
            </div>

            <div className={`form-element`}>
              <div className="form-label text-3xs">PIN Code</div>
              <Field
                type="text"
                className="form-input text-sm  border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                name="pin_code"
                placeholder="PIN Code"
              />
            </div>
          </div>
          <div className="block text-right">
            <input
              type="reset"
              className="mr-4 md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded"
              value="Reset"
            />

            <input
              type="submit"
              className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
              value="Filter"
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}
export default FilterForm;
