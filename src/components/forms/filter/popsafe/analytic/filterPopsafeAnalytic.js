import React from 'react';
import { Form, Formik, Field } from 'formik';
import { SelectReact } from '@components/forms/Input';
import { useState } from 'react';
import { useEffect } from 'react';
import Loading from '@components/loading';

function FilterPopsafeAnalytic({
  initialValues,
  onSubmit = (values, formikHelpers) => {},
  onReset,
  isLoading = false,
  data,
  user,
}) {
  const [locations, setLocations] = useState([]);
  useEffect(() => {
    if (data) {
      setLocations([{ value: '', label: 'All Location' }, ...data.lockers]);
    }
  }, [data]);

  if (isLoading) return <Loading text="please wait..." />;

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async (values, formikHelpers) => {
        onSubmit(values, formikHelpers);
      }}
      onReset={onReset}
    >
      {({ values, errors, isSubmitting, handleChange }) => (
        <Form>
          <div className="flex flex-col md:flex-row -mx-3">
            <div className="md:w-1/2 px-3">
              <div className={`form-element`}>
                <div className="form-label text-3xs">Locations</div>

                <Field
                  className="custom-select text-sm"
                  name="lockerName"
                  options={locations}
                  component={SelectReact}
                  placeholder=""
                  isMulti={false}
                />
              </div>
            </div>

            <div className="md:w-1/2 px-3">
              <div className={`form-element`}>
                <div className="form-label text-3xs">Year</div>

                <Field
                  className="custom-select text-sm"
                  name="startYear"
                  options={data?.years ?? []}
                  component={SelectReact}
                  placeholder=""
                  isMulti={false}
                />
              </div>
            </div>
          </div>
          <div className="block text-right">
            <input
              type="reset"
              className="mr-4 md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded"
              value="Reset"
            />

            <input
              type="submit"
              className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
              value="Filter"
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}
export default FilterPopsafeAnalytic;
