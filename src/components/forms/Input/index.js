import Select, { components } from 'react-select';
import DatePicker from 'react-datepicker';
import { useState } from 'react';
import { formatDate } from '@helpers/formatDate';
import Flags from 'country-flag-icons/react/3x2';

const DynamicFlagIcon = ({ name }) => {
  name = name.toUpperCase();
  const IconFlag = Flags[name];

  if (!IconFlag) {
    // Return a default on
    return null;
  }
  return <IconFlag title={name} className="w-7 h-7 mr-3" />;
};

export function SelectReact({
  className,
  placeholder,
  field,
  form,
  options,
  name,
  isMulti = false,
  tabIndex = 0,
}) {
  const singleOption = (props) => (
    <components.Option {...props}>
      <div className="flex items-center">
        {props.data.country_code || props.data.countryCode ? (
          <DynamicFlagIcon
            name={props.data.country_code ?? props.data.countryCode}
          />
        ) : null}
        {props.label}
      </div>
    </components.Option>
  );

  const singleValue = (props) => (
    <components.SingleValue {...props}>
      <div className="flex items-center">
        {props.data.country_code || props.data.countryCode ? (
          <DynamicFlagIcon
            name={props.data.country_code ?? props.data.countryCode}
          />
        ) : null}
        {props.data?.label}
      </div>
    </components.SingleValue>
  );

  const multiValue = (props) => (
    <components.MultiValue {...props}>
      <div className="flex items-center">
        {props.data.country_code ? (
          <DynamicFlagIcon name={props.data.country_code} />
        ) : null}
        {props.data?.label}
      </div>
    </components.MultiValue>
  );

  const onChange = (option) => {
    if (isMulti) {
      if (!option) {
        option = [];
      }
    }

    form.setFieldValue(
      field.name,
      isMulti ? option.map((item) => item.value) : option.value,
    );
  };

  const getValue = () => {
    if (options) {
      return isMulti
        ? options.filter((option) => field.value.indexOf(option.value) >= 0)
        : options.find((option) => option.value === field.value);
    } else {
      return isMulti ? [] : '';
    }
  };

  return (
    <Select
      instanceId={`select-${field.name}`}
      className={className}
      name={field.name}
      value={getValue()}
      onChange={onChange}
      placeholder={placeholder}
      options={options}
      isMulti={isMulti}
      components={{
        Option: singleOption,
        SingleValue: singleValue,
        MultiValue: multiValue,
      }}
      tabIndex={tabIndex}
    />
  );
}

export function DateRangeReact({ className, form, field }) {
  const { value } = field;
  const split = value?.split(' - ');

  const [dateRange, setDateRange] = useState([
    new Date(split[0]),
    new Date(split[1]),
  ]);
  const [_startDate, _endDate] = dateRange;
  return (
    <DatePicker
      className={className}
      selectsRange={true}
      startDate={_startDate}
      endDate={_endDate}
      onChange={(value) => {
        if (!value[0] && !value[1]) {
          // NOTE if one of the date is empty, set the other to current date
          const startDate = new Date();
          startDate.setMonth(startDate.getMonth() - 1);
          const currentDate = new Date();

          setDateRange([startDate, currentDate]);
          form.setFieldValue(
            field.name,
            `${formatDate(startDate)} - ${formatDate(currentDate)}`,
          );
        } else {
          setDateRange(value);
          const startDate = formatDate(value[0]);
          const endDate = formatDate(value[1]);
          form.setFieldValue(field.name, `${startDate} - ${endDate}`);
        }
      }}
      maxDate={new Date()}
      isClearable={true}
    />
  );
}

export function DatePickerReact({
  className,
  form,
  field,
  startDate = new Date(),
  endDate = new Date(),
  minDate = new Date(),
  selected = new Date(),
}) {
  const [_selected, _setSelected] = useState(field.value);
  return (
    <DatePicker
      className={className}
      selectsRange={false}
      startDate={startDate}
      endDate={endDate}
      onChange={(value) => {
        _setSelected(value);
        form.setFieldValue(field.name, value);
      }}
      selected={_selected}
      minDate={minDate}
      isClearable={true}
    />
  );
}
