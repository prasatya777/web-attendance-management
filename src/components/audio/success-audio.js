import React, { forwardRef, useImperativeHandle, useRef } from 'react';

const SuccessAudio = forwardRef((props, ref) => {
  const audio = useRef(null);
  useImperativeHandle(ref, () => ({
    play() {
      audio?.current?.play();
    },

    stop() {
      audio?.current?.pause();
    },
  }));

  return (
    <audio ref={audio}>
      <source src="/audio/success.mp3" type="audio/mpeg" />
      Your browser does not support the audio element.
    </audio>
  );
});

export default SuccessAudio;
