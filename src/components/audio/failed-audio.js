import React, { forwardRef, useImperativeHandle, useRef } from 'react';

const FailedAudio = forwardRef((props, ref) => {
  const audio = useRef(null);
  useImperativeHandle(ref, () => ({
    play() {
      audio?.current?.play();
    },
  }));

  return (
    <audio ref={audio}>
      <source src="/audio/failed.mp3" type="audio/mpeg" />
      Your browser does not support the audio element.
    </audio>
  );
});

export default FailedAudio;
