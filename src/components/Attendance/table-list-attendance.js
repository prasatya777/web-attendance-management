import React from 'react';
import DefaultDatatable from '@components/datatable/default-datatable';

const ListEmployee = ({ data, isLoading }) => {
  const columns = React.useMemo(
    () => {
      if(data){
        return [
          {
            Header: 'Date',
            accessor: 'dateTime',
            Cell: ({ value }) => value,
          },
          {
            Header: 'Clock In',
            accessor: 'clockInTime',
            Cell: ({ value }) => value,
          },
          {
            Header: 'Clock Out',
            accessor: 'clockOutTime',
            Cell: ({ value }) => value,
          }
        ]
      }

      return []
    }
  );

    return <DefaultDatatable columns={columns} data={data} isLoading={isLoading} />;
};

export default ListEmployee;
