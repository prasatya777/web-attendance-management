import React, { useEffect, useRef } from 'react';
import Portal from '@components/portal';
import { FiX } from 'react-icons/fi';
import { Field, Form, Formik } from 'formik'
const ModalFilter = ({
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  onSubmit,
  initialValues,
  onReset,
}) => {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (modalRef.current.contains(event.target) || !shouldCloseOnOverlayClick) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-5xl"
              ref={modalRef}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <div className="">
                    <h2 className="text-xs text-gray-400">Attendance History</h2>
                    <h3 className="text-xl font-bold">FILTER</h3>
                  </div>
                  <button
                    className="modal-close btn btn-transparent"
                    onClick={onClose}
                  >
                    <FiX size={18} className="stroke-current" />
                  </button>
                </div>

                <div className="relative p-4 flex-auto modal-body">
                <Formik
                    initialValues={initialValues}
                    onSubmit={async (values, formikHelpers) => {
                      onSubmit(values, formikHelpers)
                    }}
                    onReset={onReset}
                  >
                    {({
                      values,
                      errors,
                      isSubmitting,
                      handleChange,
                      setValues,
                    }) => {
                      return (
                        <Form>
                          <div className="grid grid-cols-1 lg:grid-cols-3 gap-x-3">
                            <div className="form-element">
                              <div className="form-label text-xs">
                                Tanggal Attendance
                              </div>
                              <div className="flex items-center justify-between">
                                <Field
                                  type="date"
                                  name="dateTime.startDate"
                                  className="form-input text-sm  border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                                />
                                <p>-</p>
                                <Field
                                  type="date"
                                  name="dateTime.endDate"
                                  className="form-input text-sm  border border-gray-400 focus:ring-transparent rounded w-full px-3 py-2.5 focus focus:border-gray-600 focus:outline-none active:outline-none"
                                  min={values.dateTime.startDate}
                                />
                              </div>
                            </div>
                          </div>

                          <hr className="mb-5" />

                          <div className="flex gap-x-3 justify-center">
                            <input
                              type="submit"
                              className="md:w-1/6 btn btn-default cursor-pointer border border-blue-500 text-white bg-blue-500 hover:bg-blue-600 btn-rounded"
                              value="Filter"
                            />

                            <input
                              type="reset"
                              className="md:w-1/6 btn btn-default cursor-pointer border border-red-500 text-red-500 bg-white hover:bg-red-500 hover:text-white btn-rounded"
                              value="Reset"
                            />
                          </div>
                        </Form>
                      )
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalFilter;
