import React from 'react';
import Link from 'next/link';
import Datatable from './datatable/datatable';
import { formatNominalWithCurrency } from '@helpers/formatRupiah';
import getColorByStatus from '@helpers/colorByStatusTransaction';

import { getFullDateFromISO, getFullTimeFromISO } from '@helpers/formatDate';
import getFlagIcon from '@helpers/flagIconHelper';

const ListPopsafe = ({ data, isLoading, setIsModalFilterOpen }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Invoice Code',
        accessor: 'invoice_code',
        Cell: ({ value }) => {
          return (
            <Link href="/popsafe/detail/[id]" as={`/popsafe/detail/${value}`}>
              <a className="hover:text-red-500">{value}</a>
            </Link>
          );
        },
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ row }) => {
          const { original } = row;
          const { status, sub_status: subStatus } = original;

          return (
            <div className="block">
              <div>
                <p
                  className={`inline-block rounded px-1 text-xs ${getColorByStatus(
                    status
                  )}`}
                >
                  {status}
                </p>
              </div>

              {subStatus && (
                <p className="bg-gray-200 rounded px-1 inline-block mt-1 text-xs">
                  {subStatus}
                </p>
              )}
            </div>
          );
        },
      },
      {
        Header: 'Customer',
        accessor: 'name',
        Cell: ({ value }) => value ?? '-',
      },
      {
        Header: 'Location',
        accessor: 'locker_name',
        Cell: ({ row }) => {
          const { original } = row;
          const { country, locker_name: lockerName } = original;
          return (
            <div className="flex items-center">
              {getFlagIcon({ name: country })}
              {lockerName}
            </div>
          );
        },
      },
      {
        Header: 'Locker',
        accessor: 'locker_size',
        Cell: ({ row }) => {
          const { original } = row;
          const { locker_size: lockerSize, locker_number: lockerNumber } =
            original;

          return `${lockerSize} / ${lockerNumber}`;
        },
      },
      {
        Header: 'Transaction Date',
        accessor: 'transaction_date',
        Cell: ({ value }) => {
          return (
            <div className="block">
              <p>{getFullDateFromISO(value)}</p>
              <p>{getFullTimeFromISO(value)}</p>
            </div>
          );
        },
      },
      {
        Header: 'Duration',
        accessor: 'duration',
        Cell: ({ value }) => value,
      },
      {
        Header: 'Price',
        accessor: 'total_price',
        Cell: ({ row }) => {
          const { original } = row;
          const { country, total_price: totalPrice } = original;

          return formatNominalWithCurrency(totalPrice, country);
        },
      },
      {
        Header: 'Campaign',
        accessor: 'campaign_name',
        Cell: ({ value }) => value,
      },
    ],
    []
  );
  const items = React.useMemo(() => data, [data]);

  return (
    <>
      <Datatable
        isLoading={isLoading}
        columns={columns}
        data={items}
        setIsModalFilterOpen={setIsModalFilterOpen}
      />
    </>
  );
};

export default ListPopsafe;
