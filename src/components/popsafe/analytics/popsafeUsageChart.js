import React from 'react';
import { getColor } from '@functions/colors';
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Label,
  CartesianGrid,
  Tooltip,
  Legend,
  LabelList,
} from 'recharts';

const CustomTooltip = ({ active, payload }) => {
  // const CustomTooltip = (props) => {
  if (active && payload && payload.length > 0) {
    let { day_name, detail } = { ...payload[0].payload };
    return (
      <div className="bg-white text-gray-900 dark:bg-gray-800 dark:text-white shadow-lg rounded-lg p-2 text-xs">
        <div className="font-bold">{day_name ?? detail ?? '-'}</div>
        {payload.map((item, index) => {
          const { total, persent } = item.payload[item.name];
          return (
            <div key={index} className="flex gap-x-2">
              <p style={{ color: item.fill }}>{item.name}:</p>{' '}
              <p className="font-bold">
                {total}{' '}
                <span className="font-normal">({persent.toFixed(2)}%)</span>
              </p>
            </div>
          );
        })}
      </div>
    );
  }
  return null;
};

export const PopsafeUsageChart = ({ data, filter }) => {
  let colors = [
    { key: 'CREATED', fill: getColor('bg-gray-500') },
    { key: 'EXPIRED', fill: getColor('bg-black') },
    { key: 'CANCEL', fill: getColor('bg-yellow-500') },
    { key: 'IN STORE', fill: getColor('bg-blue-500') },
    { key: 'OVERDUE', fill: getColor('bg-red-500') },
    { key: 'COMPLETE', fill: getColor('bg-green-500') },
  ];

  const renderCustomizedLabel = (props) => {
    const index = props.index;
    const target = data[index];

    let total = 0;
    colors.map((color, i) => {
      const newTarget = target[color.key];
      if (newTarget) {
        total += Number(newTarget.total);
      }

      return Number(total);
    });

    const { x, y, width } = props;

    return (
      <text x={x + width / 2} y={y - 5} fill={'#000'} textAnchor="center">
        {total}
      </text>
    );
  };

  return (
    <div style={{ width: '100%', height: 300 }}>
      <ResponsiveContainer width={'95%'}>
        <BarChart
          data={data}
          margin={{
            top: 10,
            right: 10,
            left: 30,
            bottom: 20,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis
            dataKey="label"
            axisLine={false}
            tickLine={false}
            allowDataOverflow={true}
          ></XAxis>
          <YAxis axisLine={false} tickLine={false} width={30}>
            <Label textAnchor="middle" position="inside" angle={-90} dx={-40}>
              Number of Transactions
            </Label>
          </YAxis>
          <Tooltip
            cursor={{ fill: 'transparent' }}
            content={<CustomTooltip />}
            allowEscapeViewBox={{ x: true, y: true }}
          />
          <Legend verticalAlign="bottom" height={36} />
          {colors.map((color, i) => {
            return (
              <Bar
                key={i}
                barSize={100}
                legendType="circle"
                stackId="a"
                dataKey={`${color.key}.total`}
                name={color.key}
                fill={color.fill}
              >
                {i === colors.length - 1 && (
                  <LabelList
                    dataKey={`${color.key}.total`}
                    content={renderCustomizedLabel}
                    position="insideTop"
                    style={{ fill: 'white' }}
                  />
                )}
              </Bar>
            );
          })}
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};
