import React from 'react';
import { getColor } from '@functions/colors';
import {
  ResponsiveContainer,
  Bar,
  Line,
  XAxis,
  YAxis,
  ComposedChart,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';
import { random } from '@functions/numbers';
import faker from 'faker/locale/en_US';

const popsafeUsagePercentageGrowthChart = ({ data, filter }) => {
  const chartData = [
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
    {
      label: faker.name.firstName(),
      usage: random(0, 40),
      growth: random(0, 40),
    },
  ];

  return (
    <div style={{ width: '100%', height: 350 }}>
      <ResponsiveContainer>
        <ComposedChart
          width={400}
          height={300}
          data={chartData}
          margin={{
            top: 0,
            right: 0,
            left: 0,
            bottom: 0,
          }}
        >
          <CartesianGrid stroke="#f5f5f5" vertical={true} horizontal={true} />
          <XAxis
            dataKey="label"
            tickLine={false}
            axisLine={{ stroke: '#f5f5f5' }}
          />
          <Tooltip />
          <Legend verticalAlign="top" align="left" height={36} />
          <Bar
            radius={[10, 10, 0, 0]}
            dataKey="usage"
            barSize={50}
            fill={getColor('bg-blue-500')}
            yAxisId="left"
            legendType="circle"
            name="Usage"
            label
          />
          <Line
            dot={false}
            strokeWidth={2}
            strokeLinecap="round"
            type="monotone"
            dataKey="growth"
            stroke={getColor('bg-red-500')}
            yAxisId="right"
            legendType="rect"
            name="Growth Percentage"
            label
          />
          <YAxis
            tickLine={false}
            yAxisId="left"
            axisLine={{ stroke: '#f5f5f5' }}
            tickCount={5}
          />
          <YAxis
            tickLine={false}
            yAxisId="right"
            orientation="right"
            stroke="#3B7AD9"
            axisLine={{ stroke: '#f5f5f5' }}
            unit="%"
            tickCount={5}
          />
        </ComposedChart>
      </ResponsiveContainer>
    </div>
  );
};

export default popsafeUsagePercentageGrowthChart;
