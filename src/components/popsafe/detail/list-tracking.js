import React from 'react';
import Datatable from '@components/popsafe/datatable/datatable';
import { getFullDateFromISO, getFullTimeFromISO } from '@helpers/formatDate';
import getColorByStatus from '@helpers/colorByStatusTransaction';

const ListTracking = ({ data, isLoading, setIsModalFilterOpen }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: '#',
        Cell: ({ row }) => row.index + 1,
      },
      {
        Header: 'Transaction Date',
        accessor: 'created_at',

        Cell: ({ value }) => {
          return (
            <div className="block">
              <p>{getFullDateFromISO(value)}</p>
              <p>{getFullTimeFromISO(value)}</p>
            </div>
          );
        },
      },
      {
        Header: 'Remarks',
        accessor: 'remarks',
        Cell: ({ value }) => <p className="whitespace-pre-line">{value}</p>,
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ row }) => {
          const { original } = row;
          const { status, sub_status: subStatus } = original;

          return (
            <div className="block">
              <div>
                <p
                  className={`inline-block rounded px-1 text-xs ${getColorByStatus(
                    status
                  )}`}
                >
                  {status}
                </p>
              </div>

              {subStatus && (
                <p className="bg-gray-200 rounded px-1 inline-block mt-1 text-xs">
                  {subStatus}
                </p>
              )}
            </div>
          );
        },
      },
    ],
    []
  );
  const items = React.useMemo(() => data, [data]);

  return (
    <>
      <Datatable
        isLoading={isLoading}
        columns={columns}
        data={items}
        setIsModalFilterOpen={setIsModalFilterOpen}
        isSearchAble={false}
        isSortAble={false}
      />
    </>
  );
};

export default ListTracking;
