import React from 'react';
import ListTracking from '@components/popsafe/detail/list-tracking';
import Loading from '@components/loading';

export default function TabTracking({ data, loading }) {
  return (
    <div className="p-4 bg-white mt-0.5">
      <ListTracking
        data={data.listTracking}
        isLoading={loading?.detailPopsafeLoading}
      />

      <div className="p-4 flex gap-x-5 bg-gray-100 mt-0.5">
        <h2 className="font-bold text-base w-1/3">Open Door History</h2>
      </div>

      <div className="p-4">
        {loading?.historyOpenDoorLoading ? (
          <Loading />
        ) : data.listOpenHistory.length > 0 ? (
          data.listOpenHistory.map((item, index) => (
            <div
              className={`flex relative justify-start items-start ${
                index === 0 ? 'opacity-100' : 'opacity-40'
              }`}
              key={index}
            >
              <div className="h-full w-6 absolute inset-0 flex items-center justify-center">
                <div className="h-full w-1 bg-gray-200 dark:bg-gray-800 pointer-events-none"></div>
              </div>
              <div
                className={`flex-shrink-0 w-6 h-6 rounded-full inline-flex items-center justify-center text-white relative z-10 font-medium text-sm circle bg-red-500`}
              ></div>
              <div className="flex-grow flex items-start flex-col pb-4">
                <div className="flex items-start justify-start px-4">
                  <div className={`flex flex-col w-full`}>
                    <div className={`text-sm font-bold`}>
                      {item?.parcel_number ?? '-'}
                    </div>
                    <div className="text-sm">{item?.description ?? '-'}</div>
                    <div className="text-3xs mt-1">
                      {item?.date_time ?? '-'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <h3 className="text-center text-gray-700 text-sm">
            Open door history not available
          </h3>
        )}
      </div>
    </div>
  );
}
