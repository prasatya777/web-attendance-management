import React from 'react';
import Datatable from '@components/popsafe/datatable/datatable';
import { formatRupiah } from '@helpers/formatRupiah';

const getColorByStatus = (status) => {
  const availableStatus = {
    PAID: 'bg-green-500 text-white',
  };

  return availableStatus[status] || '';
};

const ListTransactions = ({
  data,
  isLoading,
  setSelectedDetailTransaction,
}) => {
  const columns = React.useMemo(
    () => [
      {
        Header: '#',
        Cell: ({ row }) => row.index + 1,
      },
      {
        Header: 'Invoice ID',
        accessor: 'invoiceId',

        Cell: ({ value }) => (
          <p
            className="cursor-pointer text-gray-400 hover:text-red-500"
            onClick={() => setSelectedDetailTransaction(value)}
          >
            {value}
          </p>
        ),
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ value }) => {
          return (
            <p
              className={`inline-block rounded px-1 text-xs ${getColorByStatus(
                value
              )}`}
            >
              {value}
            </p>
          );
        },
      },
      {
        Header: 'Description',
        accessor: 'description',
        Cell: ({ value }) => <p className="whitespace-pre-line">{value}</p>,
      },
      {
        Header: 'Paid',
        accessor: 'paidAmount',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
      {
        Header: 'Promo',
        accessor: 'promoAmount',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
      {
        Header: 'Refund',
        accessor: 'refundAmount',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
    ],
    [setSelectedDetailTransaction]
  );
  const items = React.useMemo(() => data, [data]);

  return (
    <>
      <Datatable
        isLoading={isLoading}
        columns={columns}
        data={items}
        isSearchAble={false}
        isSortAble={false}
      />
    </>
  );
};

export default ListTransactions;
