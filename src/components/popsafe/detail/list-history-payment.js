import React from 'react';
import Datatable from '@components/popsafe/datatable/datatable';
import getColorByStatus from '@helpers/colorByStatusTransaction';
import { getFullDateTimeFromISO } from '@helpers/formatDate';

const ListHistoryPayment = ({ data, isLoading }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ value }) => {
          return (
            <p
              className={`inline-block rounded px-1 text-xs ${getColorByStatus(
                value
              )}`}
            >
              {value}
            </p>
          );
        },
      },
      {
        Header: 'Amount',
        accessor: 'totalAmount',
        Cell: ({ value }) => value,
      },
      {
        Header: 'Time',
        accessor: 'created_at',
        Cell: ({ value }) => getFullDateTimeFromISO(value),
      },
    ],
    []
  );
  const items = React.useMemo(() => data, [data]);

  return (
    <>
      <Datatable
        isLoading={isLoading}
        columns={columns}
        data={items}
        isSearchAble={false}
        isSortAble={false}
      />
    </>
  );
};

export default ListHistoryPayment;
