import React from 'react';
import Datatable from '@components/popsafe/datatable/datatable';
import { formatRupiah } from '@helpers/formatRupiah';
import { getFullDateFromISO, getFullTimeFromISO } from '@helpers/formatDate';

const getColorByStatus = (status) => {
  const availableStatus = {
    PAID: 'bg-green-500 text-white',
  };

  return availableStatus[status] || 'bg-gray-100';
};

const ListHistories = ({ data, isLoading }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'User',
        accessor: 'user',

        Cell: ({ value }) => value,
      },
      {
        Header: 'Description',
        accessor: 'description',
        Cell: ({ value }) => <p className="whitespace-pre-line">{value}</p>,
      },
      {
        Header: 'Date',
        accessor: 'transaction_date',

        Cell: ({ value }) => {
          return (
            <div className="block">
              <p>{getFullDateFromISO(value)}</p>
              <p>{getFullTimeFromISO(value)}</p>
            </div>
          );
        },
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ value }) => {
          return (
            <p
              className={`inline-block rounded px-1 text-xs ${getColorByStatus(
                value
              )}`}
            >
              {value}
            </p>
          );
        },
      },

      {
        Header: 'Paid',
        accessor: 'paid',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
      {
        Header: 'Promo',
        accessor: 'promo',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
      {
        Header: 'Refund',
        accessor: 'refund',
        Cell: ({ value }) => `Rp ${formatRupiah(value)}`,
      },
    ],
    []
  );
  const items = React.useMemo(() => data, [data]);

  return (
    <>
      <Datatable
        isLoading={isLoading}
        columns={columns}
        data={items}
        isSearchAble={false}
        isSortAble={false}
      />
    </>
  );
};

export default ListHistories;
