import Loading from '@components/loading';
import getColorByStatus from '@helpers/colorByStatusTransaction';
import { getFullDateTimeFromISO } from '@helpers/formatDate';
import React from 'react';

export default function TabSmsHistory({ data, loading }) {
  return (
    <div className="p-4 bg-white mt-0.5">
      {loading && <Loading />}

      {!loading &&
        (data?.listSmsHistory?.length > 0 ? (
          data?.listSmsHistory?.map((item, index) => (
            <div
              className={`flex relative justify-start items-start ${
                index === 0 ? 'opacity-100' : 'opacity-40'
              }`}
              key={index}
            >
              <div className="h-full w-6 absolute inset-0 flex items-center justify-center">
                <div className="h-full w-1 bg-gray-200 dark:bg-gray-800 pointer-events-none"></div>
              </div>
              <div className="flex-shrink-0 w-6 h-6 rounded-full inline-flex items-center justify-center bg-yellow-500 text-white relative z-10 font-medium text-sm"></div>
              <div className="flex-grow flex items-start flex-col pb-4">
                <div className="flex items-start justify-start px-4">
                  <div className="w-full">
                    <p
                      className={`px-2 rounded text-xs inline-block ${getColorByStatus(
                        item?.smsStatus
                      )}`}
                    >
                      {item?.smsStatus}
                    </p>
                    <div className="text-xs font-light mt-1">
                      {item?.smsContent ?? '-'}
                    </div>
                    <div className="text-3xs mt-1 font-semibold">
                      {item?.sentOn
                        ? getFullDateTimeFromISO(item?.sentOn)
                        : '-'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <h3 className="text-center text-gray-700 text-sm">
            SMS History not available
          </h3>
        ))}
    </div>
  );
}
