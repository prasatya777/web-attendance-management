import React from 'react';
import ListTransactions from '@components/popsafe/detail/list-transactions';
import Loading from '@components/loading';
import { getFullDateTimeFromISO } from '@helpers/formatDate';
import { formatNominalWithCurrency } from '@helpers/formatRupiah';
import ListHistories from './list-histories';
import ListHistoryPayment from './list-history-payment';

export default function TabTransaction({
  data,
  loading,
  setSelectedDetailTransaction,
}) {
  const detailTransaction = data?.detailTransactionData?.detailTransaction[0];
  return (
    <>
      <div className="p-4 bg-white mt-0.5">
        <ListTransactions
          data={data?.listTransaction ?? []}
          setSelectedDetailTransaction={setSelectedDetailTransaction}
          isLoading={loading?.detailPopsafeLoading}
        />
      </div>

      {data?.selectedDetailTransaction && (
        <div className="p-4 bg-white mt-4">
          {loading?.detailTransactionLoading && <Loading />}

          {!loading?.detailTransactionLoading && (
            <>
              <h2 className="text-sm font-bold">TRANSACTION DETAIL</h2>
              <h3 className="text-2xl mt-8 font-bold">
                {data?.detailTransactionData?.invoiceCode ?? '-'}
              </h3>
              <p className="mt-1 bg-yellow-400 uppercase inline-block px-6 py-2 font-bold rounded">
                {detailTransaction?.transactionType ?? '-'}
              </p>
              <p className="mt-1 text-gray-300 font-light">
                {detailTransaction?.transactionDate
                  ? getFullDateTimeFromISO(detailTransaction?.transactionDate)
                  : '-'}
              </p>

              <hr className="my-5" />
              <div className="detail">
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Description</h2>
                  <div className="flex flex-col w-2/3">
                    <p>{detailTransaction?.transactionDescription ?? '-'}</p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Status</h2>
                  <div className="flex flex-col w-2/3">
                    <p>
                      {formatNominalWithCurrency(
                        detailTransaction?.totalPrice ?? '0',
                        data?.detailPopsafeData?.countryPopsafe
                      )}
                    </p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Total Price</h2>
                  <div className="flex flex-col w-2/3">
                    <p>
                      {formatNominalWithCurrency(
                        detailTransaction?.totalPrice ?? '0',
                        data?.detailPopsafeData?.countryPopsafe
                      )}
                    </p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Paid Amount</h2>
                  <div className="flex flex-col w-2/3">
                    <p>
                      {formatNominalWithCurrency(
                        detailTransaction?.paidAmount ?? '0',
                        data?.detailPopsafeData?.countryPopsafe
                      )}
                    </p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Promo Amount</h2>
                  <div className="flex flex-col w-2/3">
                    <p>
                      {formatNominalWithCurrency(
                        detailTransaction?.promoAmount ?? '0',
                        data?.detailPopsafeData?.countryPopsafe
                      )}
                    </p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Refund Amount</h2>
                  <div className="flex flex-col w-2/3">
                    <p>
                      {formatNominalWithCurrency(
                        detailTransaction?.refundAmount ?? '0',
                        data?.detailPopsafeData?.countryPopsafe
                      )}
                    </p>
                  </div>
                </div>
              </div>
              <hr className="my-5" />
              <div className="histories">
                <h3 className="text-lg">Histories</h3>
                <ListHistories data={data?.listHistories ?? []} />
              </div>
              <div className="payments mt-5">
                <h3 className="text-lg">Payment</h3>
                <h4 className="text-xl font-bold mt-1">
                  {detailTransaction?.paymentRef ?? '-'}
                </h4>
                <hr className="my-3" />
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Status</h2>
                  <div className="flex flex-col w-2/3">
                    <p>{detailTransaction?.paymentStatus ?? '-'}</p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Payment Method</h2>
                  <div className="flex flex-col w-2/3">
                    <p>{detailTransaction?.method ?? '-'}</p>
                  </div>
                </div>
                <div className="pb-2 bg-white flex gap-x-5 mt-0.5">
                  <h2 className="font-bold text-sm w-1/3">Payment Reference</h2>
                  <div className="flex flex-col w-2/3">
                    <p className="text-blue-500 hover:text-blue-600 cursor-pointer">
                      {detailTransaction?.paymentReference ?? '-'}
                    </p>
                  </div>
                </div>
                <ListHistoryPayment
                  data={data?.detailTransactionData?.paymentHistory ?? []}
                />
              </div>
            </>
          )}
        </div>
      )}
    </>
  );
}
