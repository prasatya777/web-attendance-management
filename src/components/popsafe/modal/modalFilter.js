import React, { useEffect, useRef, useState } from 'react';
import Portal from '@components/portal';
import { FiX } from 'react-icons/fi';
import FilterForm from '@components/forms/filter/popsafe/filterPopsafe';
import { useUser } from '@data/useUser';
import { useFilterPopsafe } from '@data/popsafe/usePopsafe';
const ModalFilter = ({
  isOpen,
  onClose,
  shouldCloseOnOverlayClick = true,
  onSubmit,
  initialValues,
  onReset,
}) => {
  const { user } = useUser({});

  const { data, loading: loadingFilter } = useFilterPopsafe(user?.token);

  const [filterData, setFilterData] = useState(null);
  useEffect(() => {
    if (data) {
      const lockers = data?.collectLockerName.map((item) => ({
        value: item,
        label: item,
      }));
      const users = data?.collectName.map((item) => ({
        value: item,
        label: item,
      }));
      const phones = data?.collectPhone.map((item) => ({
        value: item,
        label: item,
      }));

      setFilterData({ lockers, users, phones });
    }
  }, [data]);

  const onFilterClick = (values) => {
    onSubmit(values);
  };

  const modalRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!modalRef || !modalRef.current) return false;
      if (
        modalRef.current.contains(event.target) ||
        !shouldCloseOnOverlayClick
      ) {
        return false;
      }
      onClose();
    };
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [modalRef, shouldCloseOnOverlayClick, onClose]);

  return (
    <>
      {isOpen && (
        <Portal selector="#portal">
          <div className="modal-backdrop fade-in"></div>
          <div className={`modal show`} data-background={'light'}>
            <div
              className="relative min-w-sm lg:w-8/12 w-10/12 mx-auto lg:max-w-5xl"
              ref={modalRef}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h3 className="text-sm">FILTER</h3>
                  <button
                    className="modal-close btn btn-transparent"
                    onClick={onClose}
                  >
                    <FiX size={18} className="stroke-current" />
                  </button>
                </div>

                <div className="relative p-4 flex-auto modal-body">
                  <FilterForm
                    initialValues={initialValues}
                    onSubmit={onFilterClick}
                    data={filterData}
                    isLoading={loadingFilter}
                    onReset={onReset}
                    user={user ?? null}
                  />
                </div>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </>
  );
};

export default ModalFilter;
