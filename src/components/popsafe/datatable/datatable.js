import React from 'react';
import {
  useTable,
  useSortBy,
  useRowSelect,
  useGlobalFilter,
} from 'react-table';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';
import Search from './search';
import Loading from '@components/loading';

const Datatable = ({
  columns,
  data,
  isLoading,
  setIsModalFilterOpen = () => {},
  isSearchAble = true,
  isSortAble = true,
}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    rows,
    state,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    useRowSelect
  );

  const { globalFilter } = state;

  // Render the UI for your table
  return (
    <>
      {isSearchAble && (
        <Search
          filter={globalFilter}
          setFilter={setGlobalFilter}
          setIsModalFilterOpen={setIsModalFilterOpen}
        />
      )}

      <div className="overflow-x-scroll">
        <table {...getTableProps()} className="table striped no-border">
          <thead className="bg-gray-200" style={{ height: 50 }}>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps(
                      isSortAble ? column.getSortByToggleProps() : ''
                    )}
                  >
                    <div className="flex flex-row items-center justify-start whitespace-nowrap">
                      <span className="font-bold">
                        {column.render('Header')}
                      </span>
                      <span className="ml-auto">
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <FiChevronDown className="stroke-current text-2xs" />
                          ) : (
                            <FiChevronUp className="stroke-current text-2xs" />
                          )
                        ) : (
                          ''
                        )}
                      </span>
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {isLoading ? (
              <tr>
                <td
                  style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                  colSpan={99}
                >
                  <Loading text="" />
                </td>
              </tr>
            ) : rows.length > 0 ? (
              rows.map((row) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    ))}
                  </tr>
                );
              })
            ) : (
              <tr>
                <td
                  style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                  colSpan={99}
                >
                  No Data
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Datatable;
