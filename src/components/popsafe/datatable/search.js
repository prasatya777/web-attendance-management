import { FiSearch } from 'react-icons/fi';

import Dropdown from '@components/dropdowns/dropdown-1';

export default function Search({ filter, setFilter, setIsModalFilterOpen }) {
  return (
    <div className="flex flex-col lg:flex-row lg:justify-between gap-y-2 lg:gap-y-0 lg:gap-x-5 items-center mb-5 bg-gray-50 p-3">
      <div className="relative w-full lg:w-9/12">
        <input
          className="pl-10 pr-5 h-10 text-xs shadow appearance-none border rounded w-full py-2 px-3 text-gray-700
          leading-tight focus:outline-none focus:shadow-outline"
          placeholder="Search for a invoice code, customer, order, location or something..."
          value={filter || ''}
          onChange={(e) => setFilter(e.target.value)}
        />
        <div type="submit" className="absolute top-0 mt-3 left-0 ml-4">
          <FiSearch className="stroke-current h-4 w-4" />
        </div>
      </div>

      <div className="w-full lg:w-3/12 flex flex-col lg:flex-row gap-y-2 lg:gap-y-0 ">
        <div className="w-full lg:w-1/3 lg:mr-3">
          <button
            className="w-full rounded-md bg-blue-100 text-sm p-1 lg:p-3"
            onClick={() => {
              setIsModalFilterOpen(true);
            }}
          >
            Filters
          </button>
        </div>
        <div className="w-full lg:w-1/3">
          <div className="relative ">
            <Dropdown
              width="w-40"
              buttonClass="w-full rounded-md bg-blue-100 text-sm p-1 lg:p-3"
              placement={`bottom-end`}
              title={`Export`}
            >
              <ul className="list-none cursor-pointer">
                <li className="dropdown-item">
                  <button
                    className="flex flex-row items-center justify-start h-10 w-full px-2 py-3"
                    onClick={() => {
                      console.log('here');
                    }}
                  >
                    <span className="mx-2">Export as .xlsx</span>
                  </button>
                </li>
                <li className="dropdown-item">
                  <button
                    className="flex flex-row items-center justify-start h-10 w-full px-2 py-3"
                    onClick={() => {
                      console.log('export .doc');
                    }}
                  >
                    <span className="mx-2">Export as .doc</span>
                  </button>
                </li>
              </ul>
            </Dropdown>
          </div>
        </div>
      </div>
    </div>
  );
}
