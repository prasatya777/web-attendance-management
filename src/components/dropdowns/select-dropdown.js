import React, { Children, useState, useRef, useEffect } from 'react';
import propTypes from 'prop-types';

export default function Select({
  labelName,
  id,
  name,
  value,
  className,
  children,
  onClick,
  fallbackText,
  direction = 'left',
}) {
  const [toggle, setToggle] = useState(() => false);
  const selectWrapper = useRef(null);

  const items = Children.toArray(children);

  function toggleSelect() {
    setToggle(() => !toggle);
  }

  function clickOutside(event) {
    if (selectWrapper && !selectWrapper.current.contains(event.target))
      setToggle(false);
  }

  useEffect(() => {
    window.addEventListener('mousedown', clickOutside);
    return () => {
      window.removeEventListener('mousedown', clickOutside);
    };
  }, []);

  const selected = items.find((item) => item.props.value === value);

  return (
    <div className={['flex', className].join(' ')}>
      {labelName && (
        <label htmlFor="" className="show text-lg mb-2 text-gray-900">
          {labelName}
        </label>
      )}

      <div className="relative" ref={selectWrapper} onClick={toggleSelect}>
        <div
          className="flex items-center justify-between cursor-pointer focus:outline-none transition-all duration-200 px-2  w-full 
         rounded-md border text-sm py-2"
        >
          <span className={`text-black`}>
            {selected?.props.children ?? fallbackText}
          </span>

          <img
            className="transform ml-5"
            src="data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 20 20'%3e%3cpath stroke='%236b7280' stroke-linecap='round' stroke-linejoin='round' stroke-width='1.5' d='M6 8l4 4 4-4'/%3e%3c/svg%3e"
            alt="arrow-icon"
            width={20}
            height={20}
          />

          <div
            className={[
              `absolute ${
                direction === 'left' ? 'left-0' : 'right-0'
              } top-9 bg-white border rounded-xl border-gray-200 py-3 w-full z-50`,
              toggle ? '' : 'hidden',
            ].join(' ')}
            style={{ minWidth: 175 }}
          >
            {items.map((item, index) => {
              return (
                <div
                  key={index}
                  className="cursor-pointer px-4 py-1 bg-white hover:bg-blue-100 transition-all duration-200"
                  onClick={() => {
                    onClick(item.props.value);
                  }}
                >
                  {item.props.children}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

Select.propTypes = {
  onClick: propTypes.func.isRequired,
  name: propTypes.string.isRequired,
  value: propTypes.oneOfType([propTypes.string, propTypes.number]).isRequired,
  fallbackText: propTypes.string,

  labelName: propTypes.string,
  id: propTypes.string,
  className: propTypes.string,
};
