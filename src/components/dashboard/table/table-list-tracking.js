import dynamic from 'next/dynamic';
import React from 'react';
const DefaultDatatable = dynamic(() =>
  import('@components/datatable/default-datatable'),
);

const ListTrackingPacket = ({ data, isLoading = false }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'TANGGAL',
        accessor: 'date',
        Cell: ({ value }) => value,
      },
      {
        Header: 'STATUS',
        accessor: 'status',
        Cell: ({ value }) => value,
      },
      {
        Header: 'LOKASI',
        accessor: 'location',
        Cell: ({ value }) => value,
      },
      {
        Header: 'DESKRIPSI',
        accessor: 'description',
        Cell: ({ value }) => <p className="whitespace-pre-line">{value ?? '-'}</p>,
      },
      {
        Header: 'OLEH',
        accessor: 'userAdmin',
        Cell: ({ value }) => value,
      },
    ],
    [],
  );
  return <DefaultDatatable columns={columns} data={data} isLoading={isLoading} />;
};

export default ListTrackingPacket;
