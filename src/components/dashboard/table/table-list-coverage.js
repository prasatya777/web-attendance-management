import React from 'react';
import DefaultDatatable from '@components/datatable/default-datatable';

const ListCoveragePopCenter = ({ data, isLoading }) => {
  const columns = React.useMemo(
    () => [
      {
        Header: 'NAMA LOKER',
        accessor: 'lockerName',
        Cell: ({ value }) => value,
      },
      {
        Header: 'UKURAN TERSEDIA',
        accessor: 'lockerAvailable',
        Cell: ({ value }) => {
          const propertyNames = Object.keys(value);
          return (
            <ul className="flex">
              {propertyNames.map((name, i) => (
                <li key={i} className="mr-3">{`${name.toUpperCase()}: ${
                  value[name]
                }`}</li>
              ))}
            </ul>
          );
        },
      },
    ],
    [],
  );
  return <DefaultDatatable columns={columns} data={data} isLoading={isLoading} />;
};

export default ListCoveragePopCenter;
