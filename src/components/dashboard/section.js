const Section = ({ title, description, right = null, children, classname = '' }) => {
  return (
    <div
      className={[
        'w-full p-4 rounded-lg bg-white border border-gray-100 dark:bg-gray-900 dark:border-gray-800',
        classname,
      ].join(' ')}
    >
      <div className="flex flex-row items-center justify-between">
        <div className="flex flex-col">
          <div className="text-xs md:text-sm font-light text-gray-500">{title}</div>
          <div className="text-lg md:text-xl font-bold mt-1">{description}</div>
        </div>
        {right}
      </div>
      {children}
    </div>
  );
};

export default Section;
