import { Bar } from 'react-chartjs-2';
import { getColor, toRGB } from '../../functions/colors';

const ViewAverageChart = ({
  height = 200,
  barColor = 'bg-red-500',
  data = {},
}) => {
  const chartData = [];
  for (const key in data) {
    chartData.push(data[key]);
  }

  const legend = {
    display: false,
    position: 'bottom',
    labels: {
      fontColor: getColor('text-gray-900'),
      boxWidth: 10,
      fontSize: 11,
    },
  };

  const options = {
    tooltips: {
      mode: 'index',
      intersect: false,
      callbacks: {
        title: (tooltipItem, data) => {
          const { label } = tooltipItem[0];
          return label;
        },
        label: (tooltipItem, data) => {
          const { value } = tooltipItem;
          return `view: ${value}`;
        },
      },
    },
    hover: {
      mode: 'nearest',
      intersect: true,
    },
    animation: {
      duration: 0,
    },
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 10,
        top: 10,
        bottom: 10,
      },
    },
    scales: {
      xAxes: [
        {
          //display: false,
          stacked: true,
          ticks: {
            fontColor: getColor('text-gray-900'),
            min: 0,
            maxTicksLimit: 20,
          },
          gridLines: {
            drawBorder: false,
            display: false,
            color: 'rgba(0, 0, 0, 0)',
          },
        },
      ],
      yAxes: [
        {
          stacked: true,
          ticks: {
            fontColor: getColor('text-gray-900'),
            min: 0,
            maxTicksLimit: 10,
          },
          gridLines: {
            drawBorder: false,
            display: false,
            color: 'rgba(0, 0, 0, 0)',
          },
        },
      ],
    },
    parsing: {
      xAxisKey: 'label',
      yAxisKey: 'view',
    },
  };

  const datas = {
    labels: chartData.map((data) => data.label),
    datasets: [
      {
        label: 'View',
        backgroundColor: toRGB(getColor(barColor), 1),
        borderColor: toRGB(getColor(barColor), 1),
        data: chartData.map((data) => data.view),
        barThickness: 20,
      },
    ],
  };

  return (
    <div style={{ height: height }}>
      <Bar
        data={datas}
        height={height}
        options={options}
        legend={legend}
        tool
      />
    </div>
  );
};

export default ViewAverageChart;
