const SectionTitle = ({ title, subtitle, right = null, color = null }) => {
  return (
    <div className="w-full pt-3">
      <div className="flex flex-row items-center justify-between mb-4">
        <div className="flex flex-col">
          <div className={`text-xs font-light `}>{title}</div>
          <div
            className={`text-xl font-bold ${
              color ? `text-${color}` : 'text-gray-500'
            }`}
          >
            {subtitle}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTitle;
