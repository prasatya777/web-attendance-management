import dynamic from 'next/dynamic';
import { getCurrentDate } from '@helpers/formatDate';
import { shallowEqual, useSelector } from 'react-redux';

const SectionTitle = dynamic(() =>
  import('@components/dashboard/section-title'),
);

const PageDashboard = () => {
  const { popCenter } = useSelector(
    (state) => ({
      config: state.config,
      popCenter: state.popCenter,
    }),
    shallowEqual,
  );

  return (
    <>
      <SectionTitle
        title={getCurrentDate()}
        subtitle={popCenter?.value?.name}
      />

      <div className="grid lg:grid-cols-4 gap-x-3 gap-y-3 w-full mb-2 lg:mb-5">

      </div>

    </>
  );
};
export default PageDashboard;
