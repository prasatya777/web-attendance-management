import React from 'react';
import { useTable, useSortBy, useRowSelect } from 'react-table';
import { FiChevronDown, FiChevronUp } from 'react-icons/fi';

const Datatable = ({ columns, data }) => {
  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, rows } =
    useTable(
      {
        columns,
        data,
      },
      useSortBy,
      useRowSelect
    );

  // Render the UI for your table
  return (
    <>
      <table {...getTableProps()} className="table striped no-border">
        <thead className="bg-gray-200" style={{ height: 50 }}>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  <div className="flex flex-row items-center justify-start whitespace-nowrap">
                    <span className="font-bold">{column.render('Header')}</span>
                    <span className="ml-auto">
                      {column.isSorted ? (
                        column.isSortedDesc ? (
                          <FiChevronDown className="stroke-current text-2xs" />
                        ) : (
                          <FiChevronUp className="stroke-current text-2xs" />
                        )
                      ) : (
                        ''
                      )}
                    </span>
                  </div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.length > 0 ? (
            rows.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => (
                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  ))}
                </tr>
              );
            })
          ) : (
            <tr>
              <td
                style={{ textAlign: 'center', backgroundColor: '#f4f4f4' }}
                colSpan={99}
              >
                No Data
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </>
  );
};

export default Datatable;
