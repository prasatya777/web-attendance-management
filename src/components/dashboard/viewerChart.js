import React from 'react';
import { getColor } from '../../functions/colors';
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Label,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';

const CustomTooltip = ({ active, payload }) => {
  // const CustomTooltip = (props) => {
  if (active && payload && payload.length > 0) {
    let { view, day_name, detail } = { ...payload[0].payload };
    return (
      <div className="bg-white text-gray-900 dark:bg-gray-800 dark:text-white shadow-lg rounded-lg p-2 text-xs">
        <div className="font-bold">{day_name ?? detail ?? '-'}</div>
        <div>
          <span className="font-bold">Viewer:</span>{' '}
          <span className="font-normal">{view}</span>
        </div>
      </div>
    );
  }
  return null;
};

export const ViewerChart = ({ data, filter }) => {
  const chartData = [];
  for (const key in data) {
    chartData.push(data[key]);
  }
  let colors = [{ dataKey: 'view', fill: getColor('bg-blue-500') }];

  return (
    <div style={{ width: '100%', height: 300 }}>
      <ResponsiveContainer width={'95%'}>
        <BarChart
          data={chartData}
          margin={{
            top: 10,
            right: 10,
            left: 30,
            bottom: 20,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis
            dataKey="label"
            axisLine={false}
            tickLine={false}
            allowDataOverflow={true}
          >
            <Label textAnchor="middle" position="outside" y={0} dy={20}>
              {filter}
            </Label>
          </XAxis>
          <YAxis axisLine={false} tickLine={false} width={30}>
            <Label textAnchor="middle" position="inside" angle={-90} dx={-40}>
              SUM of viewers
            </Label>
          </YAxis>
          <Tooltip
            content={<CustomTooltip />}
            cursor={{ fill: 'transparent' }}
          />
          <Legend verticalAlign="top" height={36} />

          {colors.map((color, i) => {
            return (
              <Bar
                key={i}
                legendType="circle"
                stackId="a"
                dataKey={color.dataKey}
                fill={color.fill}
                label
              />
            );
          })}
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};
