export default function popcenter(
  state = {
    value: 'ID_POPCENTER',
  },
  action,
) {
  switch (action.type) {
    case 'SET_POPCENTER_LOCATION':
      return {
        ...state,
        value: action.value,
      };
    default:
      return state;
  }
}
