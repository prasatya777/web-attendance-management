import {
  FiList,
  FiActivity,
  FiStar,
  FiCompass,
  FiShoppingCart,
} from 'react-icons/fi';

const initialState = [
  {
    title: 'Applications',
    items: [
      {
        url: '/dashboard',
        icon: <FiCompass size={20} />,
        title: 'Dashboard',
        items: [],
      },
      {
        url: '/',
        icon: <FiActivity size={20} />,
        title: 'Apps',
        items: [
          {
            url: '/social-feed',
            title: 'Social feed',
            items: [],
          },
          {
            url: '/tasks',
            title: 'Tasks',
            items: [],
          },
          {
            url: '/inbox',
            title: 'Inbox',
            items: [],
          },
          {
            url: '/kanban',
            title: 'Kanban',
            items: [],
          },
          {
            url: '/todo',
            title: 'Todo',
            items: [],
          },
        ],
      },
      {
        url: '/',
        icon: <FiList size={20} />,
        title: 'Menu levels',
        items: Array.from(Array(4).keys()).map((i) => {
          return {
            url: '/',
            title: `Level 1-${i + 1}`,
            items: Array.from(Array(4).keys()).map((j) => {
              return {
                url: '/',
                title: `Level 2-${j + 1}`,
                items: Array.from(Array(4).keys()).map((k) => {
                  return {
                    url: '/',
                    title: `Level 3-${k + 1}`,
                    items: Array.from(Array(4).keys()).map((l) => {
                      return {
                        url: '/',
                        title: `Level 4-${l + 1}`,
                        items: [],
                      };
                    }),
                  };
                }),
              };
            }),
          };
        }),
      },
      {
        url: '/',
        icon: <FiStar size={20} />,
        title: 'Demos',
        badge: {
          color: 'bg-indigo-500 text-white',
          text: 6,
        },
        items: [
          {
            url: '/demo-1',
            title: 'Light background',
            items: [],
          },
          {
            url: '/demo-2',
            title: 'Dark background',
            items: [],
          },
          {
            url: '/demo-4',
            title: 'Dark sidebar',
            items: [],
          },
          {
            url: '/demo-3',
            title: 'Small sidebar',
            items: [],
          },
          {
            url: '/demo-5',
            title: 'Dark small sidebar',
            items: [],
          },
          {
            url: '/demo-6',
            title: 'Dark navbar',
            items: [],
          },
        ],
      },
      {
        url: '/',
        icon: <FiShoppingCart size={20} />,
        title: 'E-commerce',
        items: [
          {
            url: '/e-commerce',
            title: 'Products',
            items: [],
          },
          {
            url: '/invoice',
            title: 'Invoice',
            items: [],
          },
          {
            url: '/pricing-tables',
            title: 'Pricing tables',
            items: [],
          },
        ],
      },
    ],
  },
];

export default function navigation(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
