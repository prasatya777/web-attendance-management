import Head from 'next/head';
import { useRouter } from 'next/router';
const Empty = ({ children }) => {
  const router = useRouter();
  const { pathname } = { ...router };

  const [, page] = pathname.split('/');
  let title = page
    .split('-')
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');

  if (pathname === '/') {
    title = 'Home';
  }
  return (
    <>
      <Head>
        <title>Attendance | {title}</title>
      </Head>
      {children}
    </>
  );
};

export default Empty;
