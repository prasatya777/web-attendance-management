import useSWR from 'swr';
import { fetcher } from '@utils/fetcher';


const formatParams = (params) => {
  let queryString = '';
  for (const key in params) {
    if (Array.isArray(params[key])) {
      const objectArray = params[key];
      for (const index in objectArray) {
        queryString += `&${key}[]=${objectArray[index]}`;
      }
    } else {
      queryString += `&${key}=${params[key]}`;
    }
  }

  return queryString.substring(1);
};

export const useEmployeeDetail = ({ userToken = null, params }) => {
  const usp = new URLSearchParams(params);
  const queryString = usp.toString();
  const { data, mutate, error } = useSWR(
    userToken && params.uuidEmployee
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/detail?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useGenerateUuid = (userToken = null) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/generate/uuid`, userToken ?? null]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useConfigUser = ({ userToken = null, params }) => {
  const usp = new URLSearchParams(params);
  const queryString = usp.toString();
  const { data, mutate, error } = useSWR(
    userToken && params?.nameConfig
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/configUserLevel?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useEmployeeList = (userToken = null, params) => {  
  const queryString = formatParams(params);
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/list?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};