import useSWR from 'swr';
import { fetcher } from '@utils/fetcher';

const formatParams = (params) => {
  let queryString = '';
  for (const key in params) {
    if (Array.isArray(params[key])) {
      const objectArray = params[key];
      for (const index in objectArray) {
        queryString += `&${key}[]=${objectArray[index]}`;
      }
    } else {
      queryString += `&${key}=${params[key]}`;
    }
  }

  return queryString.substring(1);
};

export const useGeneratePopCenterID = (userToken = null) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/getCreate`, userToken ?? null]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useGetCountryUser = (userToken = null) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/regions/countries`, userToken ?? null]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

// Address
export const useGetProvinces = ({ userToken }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/regions/provinces`, userToken ?? null]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};
export const useGetCities = ({ userToken, provinceId }) => {
  const { data, mutate, error } = useSWR(
    userToken && provinceId
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/regions/cities?provinceId=${provinceId}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};
export const useGetDistricts = ({ userToken, cityId }) => {
  const { data, mutate, error } = useSWR(
    userToken && cityId
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/regions/districts?cityId=${cityId}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};
export const useGetSubDistricts = ({ userToken, districtId }) => {
  const { data, mutate, error } = useSWR(
    userToken && districtId
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/regions/subDistricts?districtId=${districtId}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useLockerLocation = ({ userToken = null }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/lockerLocation`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useListPic = ({ userToken = null }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/getPic`, userToken ?? null]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useListPopCenter = ({ userToken = null, params }) => {
  const queryString = formatParams(params);
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/listPopCenter?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useFilterListLocation = ({ userToken = null }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/listPopCenter/getFilter`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useLocationDetail = ({ userToken = null, params }) => {
  const usp = new URLSearchParams(params);
  const queryString = usp.toString();
  const { data, mutate, error } = useSWR(
    userToken && params.uuidPopCenter
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/detail?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};

export const usePopCenterListCollect = ({ userToken = null }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/popCenter/getCollect`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {},
  );

  return { data, mutate, error, loading: !data && !error };
};
