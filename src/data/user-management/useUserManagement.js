import useSWR from 'swr';
import { fetcher } from '@utils/fetcher';

const formatParams = (params) => {
  let queryString = '';
  for (const key in params) {
    if (Array.isArray(params[key])) {
      const objectArray = params[key];
      for (const index in objectArray) {
        queryString += `&${key}[]=${objectArray[index]}`;
      }
    } else {
      queryString += `&${key}=${params[key]}`;
    }
  }

  return queryString.substring(1);
};

export const useUserManagementList = (userToken = null, params) => {
  const queryString = formatParams(params);
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/list?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useDetailUserById = (userToken = null, params) => {
  const usp = new URLSearchParams(params);
  const queryString = usp.toString();
  const { data, mutate, error } = useSWR(
    userToken && params.id
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/detail?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useListDivision = ({ userToken = null }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [`${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/user/listUserLevel`, userToken ?? null]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useCollectFilter = ({ userToken = null }) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/list/getFilter`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};

export const useConfigUserLevel = ({ userToken = null, params }) => {
  const usp = new URLSearchParams(params);
  const queryString = usp.toString();
  const { data, mutate, error } = useSWR(
    userToken && params?.nameConfig
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/employees/configUserLevel?${queryString}`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
    },
  );

  return { data, mutate, error, loading: !data && !error };
};
