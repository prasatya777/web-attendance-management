import useSWR from 'swr';
// import { fetcher } from '@utils/fetcher';
// import { useState } from 'react';

// const fetchWithHeaders = async (url, headers) => {
//   const response = await fetch(url, {
//     headers: {
//       ...headers,
//       'API-HEADER-AUTH': `${process.env.NEXT_PUBLIC_API_ATTENDANCE_HEADER_AUTH}`, // Update with your new headers
//     },
//   });
//   return response.json();
// };

const formatParams = (params) => {
  let queryString = ''
  for (const key in params) {
    if (Array.isArray(params[key])) {
      const objectArray = params[key]
      for (const index in objectArray) {
        queryString += `&${key}[]=${objectArray[index]}`
      }
    } else if (typeof params[key] === 'object') {
      const object = params[key]
      if (object.startDate !== '' && object.endDate !== '') {
        queryString += `&${key}=${object.startDate} - ${object.endDate}`
      } else {
        queryString += ''
      }
    } else {
      queryString += `&${key}=${params[key]}`
    }
  }

  return queryString.substring(1)
}

export const useAttendanceHistory = (userToken = null, params) => {
  const queryString = formatParams(params);
  const fetchKey = userToken
    ? [
        `${process.env.NEXT_PUBLIC_API_ATTENDANCE_URL}/attendance/histories?${queryString}`,
        userToken ?? null,
      ]
    : null;

  const { data, mutate, error } = useSWR(
    fetchKey,
    async (url, token) => {
      const response = await fetch(url, {
        headers: {
          Authorization: `Bearer ${token}`,
          'API-HEADER-AUTH': `${process.env.NEXT_PUBLIC_API_ATTENDANCE_HEADER_AUTH}`
        },
      });
      return response.json();
    },
    {
      shouldRetryOnError: false,
    }
  );

  return { data, mutate, error, loading: !data && !error };
};