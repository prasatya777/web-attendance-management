import useSWR from 'swr';
import { fetcher } from '@utils/fetcher';

export const useFilter = (userToken = null) => {
  const { data, mutate, error } = useSWR(
    userToken
      ? [
          `${process.env.NEXT_PUBLIC_API_EMPLOYEE_URL}/analytic/averageViewer/collectFilter`,
          userToken ?? null,
        ]
      : null,
    fetcher,
    {
      shouldRetryOnError: false,
    }
  );

  return { data, mutate, error, loading: !data && !error };
};
