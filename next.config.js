/** @type {import('next').NextConfig} */

const withPlugins = require('next-compose-plugins');
const withReactSvg = require('next-react-svg');
const withImages = require('next-images');

const path = require('path');

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withPlugins(
  [
    withImages({}),
    withReactSvg({
      include: path.resolve(__dirname, './public/images'),
      webpack(config, options) {
        return config;
      },
    }),
  ],
  { pageExtensions: ['mdx', 'md', 'jsx', 'js', 'tsx', 'ts'] },
  withBundleAnalyzer({}),
);
